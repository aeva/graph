# Declare unit tests Usage:
#
# unit_tests(
#   LABEL <prefix for all unit tests>
#   SOURCES <test_source_list>
#   SOURCES_REQUIRE_DATA <test_sources_that_require_DATA_DIR>
#   EXTRA_SOURCES <helper_source_files>
#   LIBRARIES <dependent_library_list>
#   )
function(unit_tests)
  set(options)
  set(oneValueArgs)
  set(multiValueArgs LABEL SOURCES SOURCES_REQUIRE_DATA EXTRA_SOURCES LIBRARIES)
  cmake_parse_arguments(ut
    "${options}" "${oneValueArgs}" "${multiValueArgs}"
    ${ARGN}
    )

  list(APPEND ut_SOURCES ${ut_SOURCES_REQUIRE_DATA})

  list(LENGTH ut_SOURCES num_sources)
  if(NOT ${num_sources})
    # without source files, don't make a target
    return()
  endif()

  if (AEVA_ENABLE_TESTING)
    smtk_get_kit_name(kit)
    #we use UnitTests_ so that it is a unique key to exclude from coverage
    set(test_prog UnitTests_${kit})

    create_test_sourcelist(TestSources ${test_prog}.cxx ${ut_SOURCES})
    add_executable(${test_prog} ${TestSources} ${ut_EXTRA_SOURCES})

    target_link_libraries(${test_prog} LINK_PRIVATE ${ut_LIBRARIES})
    target_include_directories(${test_prog}
      PRIVATE
        ${CMAKE_CURRENT_BINARY_DIR}
        ${MOAB_INCLUDE_DIRS}
        ${VTK_INCLUDE_DIRS}
    )

    target_compile_definitions(${test_prog} PRIVATE "AEVA_DATA_DIR=\"${PROJECT_SOURCE_DIR}/data\"")
    target_compile_definitions(${test_prog} PRIVATE "AEVA_SCRATCH_DIR=\"${CMAKE_BINARY_DIR}/Testing/Temporary\"")

    foreach (test ${ut_SOURCES})
      get_filename_component(tname ${test} NAME_WE)
      add_test(NAME ${tname}
        COMMAND ${test_prog} ${tname} ${${tname}_EXTRA_ARGUMENTS}
        )
      set_tests_properties(${tname} PROPERTIES TIMEOUT 120)
      if(ut_LABEL)
        set_tests_properties(${tname} PROPERTIES LABELS ${ut_LABEL})
      endif()
    endforeach(test)
  endif ()
endfunction(unit_tests)
