# A contract file for testing the aeva session within SMTK.

cmake_minimum_required(VERSION 3.8.2)
project(aeva-session)

include(ExternalProject)

ExternalProject_Add(aeva-session
  GIT_REPOSITORY "https://gitlab.kitware.com/aeva/session"
  GIT_TAG "origin/master"
  PREFIX plugin
  STAMP_DIR plugin/stamp
  SOURCE_DIR plugin/src
  BINARY_DIR plugin/build
  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
    -DENABLE_PYTHON_WRAPPING=ON
    -DENABLE_TESTING=ON
    -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
    -Dsmtk_DIR=${smtk_DIR}
    ${response_file}
  INSTALL_COMMAND ""
  TEST_COMMAND "${CMAKE_CTEST_COMMAND}" --output-on-failure
  TEST_BEFORE_INSTALL True
)
