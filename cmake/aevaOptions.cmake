option(AEVA_ENABLE_TESTING "Enable tests." ON)
if (AEVA_ENABLE_TESTING)
  include(TestingMacros)
endif()
option(AEVA_ENABLE_PYTHON "Build python bindings for aeva." OFF)
# AEVA_BUILD_DOCUMENTATION is an enumerated option:
# never  == No documentation, and no documentation tools are required.
# manual == Only build when requested; documentation tools (doxygen and
#           sphinx) must be located during configuration.
# always == Build documentation as part of the default target; documentation
#           tools are required. This is useful for automated builds that
#           need "make; make install" to work, since installation will fail
#           if no documentation is built.
set(AEVA_BUILD_DOCUMENTATION
  "never" CACHE STRING "When to build Doxygen- and Sphinx-generated documentation.")
set_property(CACHE AEVA_BUILD_DOCUMENTATION PROPERTY STRINGS never manual always)
