#!/usr/bin/env python
import os
from textwrap import dedent, indent
from camelcase import CamelCase
camelCaser = CamelCase()
dedentString = lambda s : dedent(s[1:]) if s[0].isspace() else s
indentString = lambda s, n : indent(s, ' '*n, lambda ss : ss != '\n')
camelCase = lambda s : camelCaser.hump(s)

# Define a hierarchy of classes and generate files
nodeClasses = {
    'smtk::graph::Component': {
        # All markup components inherit this base class used by several arc types.
        'smtk::markup::Component': {
            'header-forward-classes': ['smtk::markup::Group', 'smtk::markup::Label'],
            'arcs': {
                'groups': { 'type': 'Groups', 'template': 'Arcs', 'dest-type': 'smtk::markup::Group' },
                'labels': { 'type': 'Labels', 'template': 'Arcs', 'dest-type': 'smtk::markup::Label' }
            },
            'members': {
                'type_index': {
                    'type': 'static const Component::Index', 'access': 'public',
                    'description': 'A unique integer corresponding to the component type.',
                },
                'name': {
                    'type': 'std::string',
                },
            },
            'internal-classes': {
                'ModifyName': {
                    'type': 'struct',
                    'description': 'A functor for changing the name of a component.',
                    'body': """
                      struct Component::ModifyName
                      {
                        ModifyName(const std::string& nextName)
                          : m_name(nextName)
                        {
                        }

                        void operator()(Component::Ptr& c)
                        {
                          c->m_name = m_name;
                        }

                        std::string m_name;
                      };
                    """
                },
            },
            'methods': {
                'index': {
                    'attributes': { 'virtual': True },
                    'signature': 'Component::Index index() const',
                    'description': 'The index is a compile-time intrinsic of the derived resource; as such, it cannot be set.',
                    'body': """return std::type_index(typeid(*this)).hash_code();""",
                },
                'name': {
                    'signature': 'std::string name() const override',
                    'description': 'Return the component\'s name.',
                    'body': """
                      return m_name;
                    """,
                },
                'setName': {
                    'signature': 'bool setName(const std::string& name)',
                    'description': """Set the component's name.""",
                    'body': """
                        if (name == m_name)
                        {
                          return false;
                        }

                        auto owner = std::dynamic_pointer_cast<smtk::markup::Resource>(this->resource());
                        if (owner)
                        {
                          // We are not allowed to set our name directly since
                          // our owning resource has indexed us by name.
                          return owner->modifyComponent(*this, ModifyName(name));
                        }
                        m_name = name;
                        return true;
                    """,
                }
            },
            'type-aliases': {
                'Index': 'std::size_t'
            },
            'source-includes': [
                'smtk/markup/Resource.h',
            ],
            # Indexing components
            # These components are used to enumerate the domain of discrete and parametric datasets.
            'smtk::markup::Domain': {
                'description': 'The domain of a discrete or parameterized dataset.',
                # Discrete datasets refer to their degrees of freedom (DOFs) with integers.
                'smtk::markup::IdSpace': {
                    'description': """
                      A numbering used in a context.
                      Examples: node numbers, element numbers, side numbers, pedigree IDs, global IDs.""",
                    'type-aliases': {
                        'IdType': 'std::size_t'
                    },
                    'members': {
                        'range': { 'type': 'std::array<IdSpace::IdType, 2>', 'access': 'read-only' }
                    },
                    'header-includes': [ { 'system': True, 'name': 'array' } ],
                },
                # Parametric datasets enumerate their shape with continuous maps from a parameter space into world coordinates.
                'smtk::markup::ParameterSpace': {
                    'description': 'A dataset whose spatial extents serve as the domain of a map into a different coordinate system.',
                    'members': {
                        'data': { 'type': 'std::weak_ptr<smtk::markup::DiscreteGeometry>' }
                    },
                    'header-forward-classes': ['smtk::markup::DiscreteGeometry'],
                },
                # Reverse lookups from Field data into the domain use an index to accelerate queries that would otherwise traverse the entire dataset.
                'smtk::markup::Index': {
                    'description': 'A catalog that orders numbers in an IdSpace according to some field. Example: A map from a range of Field values to an IdSpace representing points of a dataset.'
                },
                'smtk::markup::BoundaryOperator': {
                    'description': 'A discrete mapping from one IdSpace into another that enumerates boundaries of the domain.'
                }
            },
            # Within an IdSpace, datasets claim an allotment of integers.
            # The allotment may be:
            # + primary (owned by the dataset),
            # + referential (owned by another dataset),
            # + or non-exclusive (owned only by the resource and referenced by datasets).
            # Generally, point ids are non-exclusive and non-contiguous while
            # cell ids are primary for high-dimensional datasets and
            # referential for subsets or lower-dimensional boundaries of those datasets.
            'smtk::markup::AssignedIds': {
                'description': 'An API for querying the IDs allotted to a component in an IdSpace.',
                'type-aliases': {
                    'IdType': 'smtk::markup::IdSpace::IdType',
                    'IdIterator': 'struct { smtk::markup::IdSpace* idSpace; IdType begin; IdType end; Nature nature; }',
                    'IdRange': 'struct { smtk::markup::IdSpace* idSpace; IdType begin; IdType end; }',
                    'Iterable': 'std::function<bool(IdIterator&)>',
                    'ContainsFunctor': 'std::function<std::size_t(const IdRange&)>',
                    'Visitor': 'std::function<smtk::common::Visit(smtk::markup::IdSpace&, IdType)>'
                },
                'enums': {
                    'Nature': {
                        'Primary': { },
                        'Referential': { },
                        'NonExclusive': { },
                    },
                },
                'header-includes': [
                    'smtk/common/Visit.h',
                    'smtk/markup/IdSpace.h',
                ],
                'methods': {
                    'range' : {
                        'signature': 'std::array<AssignedIds::IdType, 2> range() const',
                        'description': 'Returns the range of IDs in the allotment.',
                        'attributes': [ 'pure virtual' ]
                    },
                    'iterable' : {
                        'signature': 'Iterable iterable() const',
                        'description': 'Returns a functor to iterate all assigned IDs. Call the functor until it returns false.',
                        'attributes': [ 'pure virtual' ]
                    },
                    'contains' : {
                        'signature': 'ContainsFunctor contains() const',
                        'description': 'Returns functor to query the number of allotted ids in the half-open interval [begin, end[.',
                        'attributes': [ 'pure virtual' ]
                    },
                    'visit' : {
                        'signature': 'smtk::common::Visit visit(Visitor visitor) const',
                        'description': 'Call \\a visitor on each alloted ID.',
                        'attributes': [ 'pure virtual' ]
                    }
                },
                'members': {
                    'space': { 'type': 'std::weak_ptr<smtk::markup::IdSpace>' },
                    'nature': { 'type': 'AssignedIds::Nature' }
                }
            },
            # Grouping components
            'smtk::markup::Group': {
                'description': 'A set of components owned by this collection (i.e., held by shared pointer).',
                'members': {
                    'keys': { 'type': 'std::weak_ptr<smtk::markup::AssignedIds>' },
                    'ownsMembers': { 'type': 'bool', 'description': 'When removing a member arc, should the component be deleted?' },
                },
                'arcs': {
                    'members': { 'type': 'Members', 'template': 'Arcs', 'dest-type': 'smtk::markup::Component' },
                },
                'methods': {
                },
                'header-includes': [
                    'smtk/markup/AssignedIds.h'
                ],
            },
            # Organizational components
            'smtk::markup::Ontology': {
                'header-includes': [
                    'smtk/markup/IdSpace.h',
                ],
                'type-aliases': {
                    'IdType': 'smtk::markup::IdSpace::IdType'
                },
                'description': 'A collection of labels related to one another by relationships, typically imported from an OWL',
                'members': {
                    'url': { 'type': 'std::string' },
                }
            },
            # Spatial components
            'smtk::markup::SpatialData': {
                'smtk::markup::Frame': {
                    'description': 'A coordinate frame (i.e., a set of orthonormal basis vectors that span a space).',
                    'members': {
                        'axes': { 'type': 'std::vector<std::vector<double>>' }
                    }
                },
                'smtk::markup::AnalyticShape': {
                    'description': 'Simple shapes that have analytic representations, generally as implicit, trivariate functions whose zero level-set defines the shape.',
                    'smtk::markup::Plane': {
                        'description': 'A flat 3-dimensional surface identified by a point and normal.',
                        'members': {
                            'basePoint': { 'type': 'std::array<double, 3>' },
                            'normal': { 'type': 'std::array<double, 3>' }
                        }
                    },
                    'smtk::markup::Sphere': {
                        'description': 'A 3-dimensional surface of constant non-zero curvature identified by a point and radius.',
                        'members': {
                            'center': { 'type': 'std::array<double, 3>' },
                            'radius': { 'type': 'std::array<double, 3>' }
                        }
                    },
                    'smtk::markup::Cone': {
                        'description': 'A ruled 3-dimensional surface identified by 2 points and 2 radii.',
                        'members': {
                            'endpoints': { 'type': 'std::array<std::array<double, 3>, 2>' },
                            'radii': { 'type': 'std::array<double, 2>' }
                        }
                    },
                    'smtk::markup::Box': {
                        'description': 'A 3-dimensional, 6-sided shape with axis-aligned planar surfaces.',
                        'members': {
                            'range': { 'type': 'std::array<std::array<double, 3>, 2>' }
                        }
                    },
                    'smtk::markup::ImplicitShape': {
                        'description': 'A shape described by a function defined on a domain.',
                        'members': {
                            'domain': { 'type': 'std::weak_ptr<smtk::markup::Domain>' },
                            'function': { 'type': 'vtkSmartPointer<vtkImplicitFunction>' }
                        },
                        'header-includes': [ 'vtkSmartPointer.h' ],
                        'header-forward-classes': [ 'vtkImplicitFunction', 'smtk::markup::Domain' ],
                    }
                },
                'smtk::markup::DiscreteGeometry': {
                    'smtk::markup::Image': {
                        'description': 'An n-dimensional grid of cells.',
                        'members': {
                            'axes': { 'type': 'std::vector<std::weak_ptr<smtk::markup::AssignedIds>>', 'access': 'force-write' },
                            'points': { 'type': 'std::weak_ptr<smtk::markup::AssignedIds>' },
                            'cells': { 'type': 'std::weak_ptr<smtk::markup::AssignedIds>' },
                            'data': { 'type': 'vtkSmartPointer<vtkImageData>' }
                        },
                        'header-includes': [ 'smtk/markup/AssignedIds.h', 'vtkSmartPointer.h' ],
                        'header-forward-classes': [ 'vtkImageData' ],
                    },
                    'smtk::markup::Grid': {
                        'description': 'A base class for collections of unstructured cells.',
                        'members': {
                            'points': { 'type': 'std::weak_ptr<smtk::markup::AssignedIds>' },
                            'cells': { 'type': 'std::weak_ptr<smtk::markup::AssignedIds>' }
                        },
                        'header-includes': [ 'smtk/markup/AssignedIds.h' ],
                        'smtk::markup::VolumeGrid': {
                            'description': 'A collection of unstructured, possibly-nonconforming cells of any dimension.',
                            'members': {
                                'data': { 'type': 'vtkSmartPointer<vtkUnstructuredGrid>' }
                            },
                            'header-includes': [ 'vtkSmartPointer.h' ],
                            'header-forward-classes': [ 'vtkUnstructuredGrid' ],
                        },
                        'smtk::markup::SurfaceGrid': {
                            'description': 'A collection of unstructured, possibly-nonconforming surface cells.',
                            'members': {
                                'data': { 'type': 'vtkSmartPointer<vtkPolyData>' }
                            },
                            'header-includes': [
                                { 'system': True, 'name': 'memory' },
                                '---',
                                'vtkSmartPointer.h',
                            ],
                            'header-forward-classes': [ 'vtkPolyData' ]
                        }
                    }
                },
                # Spatial components
                'smtk::markup::Subset': {
                    'description': 'A subset of an IdSpace. Subsets may represent cell or node subsets. See SideSet for subsets that involve the boundary operator.',
                    'header-includes': [ 'smtk/markup/AssignedIds.h' ],
                    'type-aliases' : {
                        'Visitor': 'std::function<smtk::common::Visit(smtk::markup::IdSpace&, smtk::markup::IdSpace::IdType)>'
                    },
                    'members': {
                        'ids': {
                            'type': 'std::weak_ptr<smtk::markup::AssignedIds>',
                            'description': 'The IdSpace which is being subsetted.'
                        }
                    },
                    'methods': {
                        'visit': {
                            'signature': 'smtk::common::Visit visit(Visitor visitor)',
                            'description': 'The signature of functions that visit elements of a subset.',
                            'attributes': [ 'pure virtual' ]
                        }
                    },
                    'smtk::markup::ExplicitSubset': {
                        'description': 'A subset whose members are explicitly listed.',
                        'members': {
                            'members': { 'type': 'std::set<smtk::markup::IdSpace::IdType>' }
                        }
                    },
                    'smtk::markup::RangedSubset': {
                        'description': 'A subset whose members are stored as ranges rather than single values.',
                        'members': {
                            'members': { 'type': 'std::set<std::array<smtk::markup::IdSpace::IdType, 2>>' }
                        }
                    },
                    'smtk::markup::CompositeSubset': {
                        'description': 'A subset whose members are stored as other subsets.',
                        'members': {
                            'members': {
                                'type': 'std::set<std::weak_ptr<smtk::markup::Subset>, std::owner_less<std::weak_ptr<smtk::markup::Subset>>>',
                                'access': 'force-write',
                            }
                        },
                        'header-forward-classes': ['smtk::markup::Subset'],
                    }
                    # ... we could add a LambdaSubset whose visit method simply invokes the visitor as it wishes.
                },
                'smtk::markup::SideSet': {
                    'description': 'An adaptation of subsets for representing subsets of boundaries of spatial data.',
                    'header-forward-classes': [ 'smtk::markup::AssignedIds' ],
                    'header-includes': [
                        'smtk/common/Visit.h',
                        'smtk/markup/BoundaryOperator.h',
                        'smtk/markup/IdSpace.h'
                    ],
                    'type-aliases' : {
                        'Visitor': 'std::function<smtk::common::Visit(smtk::markup::IdSpace&, smtk::markup::IdSpace::IdType, smtk::markup::IdSpace::IdType)>'
                    },
                    'methods': {
                        'visit': {
                            'signature': 'smtk::common::Visit visit(Visitor visitor)',
                            'description': 'The signature of functions that visit elements of a subset.',
                            'body': """
                              smtk::common::Visit result = smtk::common::Visit::Halt;
                              auto allotment = m_domain.lock();
                              if (!allotment)
                              {
                                return result;
                              }
                              auto space = allotment->space();
                              for (const auto& side : m_sides)
                              {
                                result = visitor(*space, side.first, side.second);
                                if (result == smtk::common::Visit::Halt)
                                {
                                  break;
                                }
                              }
                              return result;
                            """,
                            'attributes': [ 'virtual' ]
                        }
                    },
                    'members': {
                        'domain': { 'type': 'std::weak_ptr<smtk::markup::AssignedIds>' },
                        'boundaryOperator': { 'type': 'std::weak_ptr<smtk::markup::BoundaryOperator>' },
                        'sides': {
                            'type': 'std::multimap<smtk::markup::IdSpace::IdType, smtk::markup::IdSpace::IdType>',
                            'description': 'The keys are entries of the primary IdSpace; the values are entries of the boundary-operator\'s domain.'
                        },
                    },
                },
            },
            # Non-spatial components
            'smtk::markup::Field': {
                'header-includes': [ { 'name': 'string', 'system': True }, 'smtk/string/Token.h' ],
                'header-forward-classes': [ 'smtk::markup::AssignedIds', 'smtk::markup::Index' ],
                'members': {
                    'name': { 'type': 'smtk::string::Token' },
                    'type': { 'type': 'smtk::string::Token' }, # scalar, vector, tensor, bigfoot
                    'order': { 'type': 'std::vector<int>' }, # dimension of each tuple
                    'ids': { 'type': 'std::weak_ptr<smtk::markup::AssignedIds>' },
                    'lookup': { 'type': 'std::weak_ptr<smtk::markup::Index>' }, # reverse lookup table
                },
                'methods': {
                    'name': {
                        'signature': 'std::string name() const',
                        'attributes': { 'override': True },
                        'body': """
                          return m_name.data();
                        """
                    },
                },
                'smtk::markup::LabelMap': {
                    'header-includes': [
                        'smtk/markup/IdSpace.h'
                    ],
                    'description': 'A mapping from an IdSpace to an integer label, with optional string names for each integer.',
                    'members': {
                        'names' : { 'type': 'std::unordered_map<smtk::markup::IdSpace::IdType, smtk::string::Token>', 'description': 'A set of labels that form the image of the label-map.' },
                    },
                    'smtk::markup::Segmentation': {
                        'description': 'A label map that contains only 2 or 3 labels: 0 (outside), 1 (inside), and optionally 2 (on)'
                    },
                },
            },
            'smtk::markup::Label': {
                'arcs': {
                    'subject': { 'type': 'Subject', 'template': 'Arcs', 'dest-type': 'smtk::markup::Component' },
                },
                'smtk::markup::OntologyIdentifier': {
                    'type-aliases': {
                        'Token' : 'smtk::string::Token',
                    },
                    'header-includes': [
                        'smtk/markup/Ontology.h',
                        'smtk/string/Token.h',
                    ],
                    'members': {
                        'ontologyId': {
                            'type': 'OntologyIdentifier::Token',
                            'description': 'A URL in the ontology''s schema that can be queried for relationships.' },
                    },
                    'arcs': {
                        'ontology': { 'type': 'OntologySource', 'template': 'Arc', 'dest-type': 'smtk::markup::Ontology' },
                    },
                },
                'smtk::markup::Comment': {
                    'header-includes': [
                        'smtk/string/Token.h',
                    ],
                    'members': {
                        'data': { 'type': 'smtk::string::Token', 'description': 'Text of the comment.' },
                        'mimetype': { 'type': 'smtk::string::Token', 'description': 'Mime type of the comment. The default is "text/plain".' },
                    },
                },
            },
            'smtk::markup::Feature': {
                'smtk::markup::Landmark': {
                },
            }
        }
    }
}
arcClasses = {
    'smtk::markup::Members' : {
        'template': 'Arcs',
        'source-type': 'smtk::markup::Group',
        'dest-type': 'smtk::markup::Component',
        'inverse': 'Groups'
    },
    'smtk::markup::Groups': {
        'template': 'Arcs',
        'source-type': 'smtk::markup::Component',
        'dest-type': 'smtk::markup::Group',
        'inverse': 'Members'
    },
    # 'type-aliases': {
    # },
    'smtk::markup::DerivedData': { } # Arc from a 'promoted reference' copy of data to its original data.
}

def writeFileIfDifferent(contents, path):
    shouldWrite = False
    try:
        current = open(path, 'r')
        if contents != current.read():
            shouldWrite = True
    except:
        shouldWrite = True
    if shouldWrite:
        file = open(path, 'w')
        file.write(contents)
    return shouldWrite

def openNamespaces(ns):
    code = ''
    for name in ns:
        code += 'namespace {0}\n{{\n'.format(name)
    code +=  '\n'
    return code

def closeNamespaces(ns):
    code = ''
    for name in reversed(ns):
        code += '}} // namespace {0}\n'.format(name)
    return code

def namespacesMatch(ns, fqClassName):
    ns2 = fqClassName.split('::')[:-1]
    return ns == ns2

def classNameToHeaderName(fqClassName):
    fqClassVec = fqClassName.split('::')
    header = os.path.join(*fqClassVec)
    header += '.h'
    return header

def declareClass(fqClassName):
    fqClassVec = fqClassName.split('::')
    if len(fqClassVec) == 1:
        return 'class ' + fqClassName + ';\n'
    decl = ''
    for ns in fqClassVec[:-1]:
        decl += 'namespace {0} {{'.format(ns)
    decl += ' class ' + fqClassVec[-1] + ';'
    for ns in fqClassVec[:-1]:
        decl += ' }'
    decl += '\n'
    return decl

def includeDirectives(includeList):
    code = ''
    for incl in includeList:
        if type(incl) == str:
            if incl == '---':
                code += '\n'
            else:
                code += '#include "{0}"\n'.format(incl)
        elif 'system' in incl:
            code += '#include <{0}>\n'.format(incl['name'])
        else:
            code += '#include "{0}"\n'.format(incl['name'])

    if len(includeList) > 0:
        code += '\n'
    return code

def generateHeaderHeader(ns, name, inherit, extraIncludes, forwardClasses):
    nspath = '_'.join(ns) + '_' + name + "_h"
    header = dedentString("""
    // Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
    #ifndef {0}
    #define {0}

    """.format(nspath))
    # Add includes
    if len(inherit) <= 0 or not namespacesMatch(ns, inherit[-1]):
        # Include export macro for namespace.
        header += '#include "{0}/Exports.h"\n\n'.format(os.path.join(*ns))
    if len(inherit) > 0:
        # Include parent class
        header += '#include "{0}"\n\n'.format(classNameToHeaderName(inherit[-1]))
    header += includeDirectives(extraIncludes);

    # Add forward declarations of classes
    for fqClassName in forwardClasses:
        header += declareClass(fqClassName)
    if len(forwardClasses) > 0:
        header += '\n'

    # Add namespace open-brackets.
    header += openNamespaces(ns)
    return header

def generateHeaderFooter(ns, name):
  nspath = '_'.join(ns) + '_' + name + "_h"
  footer = '\n' + closeNamespaces(ns) + '\n'
  footer += '#endif // {0}\n'.format(nspath)
  return footer

def generateSourceHeader(ns, name):
  nspath = os.path.join(*ns, name)
  header = dedentString("""
  // Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
  #include "{0}.h"

  """.format(nspath))
  return header

def generateSourceFooter(ns, name):
  footer = '\n' + closeNamespaces(ns)
  return footer

def generateInternalClasses(entry):
    code = ''
    if 'internal-classes' not in entry:
        return code
    for internalClassName, internalClass in entry['internal-classes'].items():
        code += dedentString(internalClass['body'])
    return code

def generateCtorDtor(ns, bareName, classDef, includes):
    # TODO: Check classDef for special ctor
    code = dedentString("""
    {0}::~{0}() = default;

    """.format(bareName))
    return code

def classNameToInclude(className):
    ns = className.split('::')
    if len(ns) > 1 and ns[0] == 'std':
        return '<' + ns[1] + '.h>'
    return os.path.join(*ns) + '.h'

def bareClassName(fqClassName):
    ns = fqClassName.split('::')
    return (ns[:-1], ns[-1])

def exportMacro(fqClassName):
    return ''.join([x.upper() for x in fqClassName.split('::')[:-1]]) + '_EXPORT'

def includeScan(code, includes):
    import re
    for match in re.findall('[^ ]*::[^ (]*', code):
        # Ignore '#include' directives.
        if match.startswith('smtk') and match.find('\\.h') < 0:
            print('    *** HEADER %s ***' % match)
            includes.add(classNameToInclude(match))

def generateMethodStubs(ns, bareName, classDef, includes):
    code = ''

    methodNames = set()
    if 'methods' in classDef:
        for methodName, methodDef in classDef['methods'].items():
            if isPureVirtualMethod(methodDef):
                continue
            sig = methodDef['signature'].replace(methodName, '{0}::{1}'.format(bareName, methodName), 1).replace(' override','')
            if 'body' in methodDef:
                code += dedentString(sig) + '\n{{\n{0}}}\n\n'.format(indentString(dedentString(methodDef['body']), 2))
            else:
                code += dedentString(sig) + '\n{\n}\n\n'
            # includeScan(code, includes)
            methodNames.add(methodName)

    if 'members' in classDef:
        for memberName, memberDef in classDef['members'].items():
            mType = memberDef['type']
            if 'access' in memberDef and memberDef['access'] == 'public':
                # Do not provide setter/getter methods; access is direct.
                continue;

            explicitSetter = True if 'set%s' % camelCase(memberName) in methodNames else False
            explicitGetter = True if '%s' % memberName in methodNames else False
            # Add a "set" method
            if not explicitSetter and ('access' not in memberDef or memberDef['access'] != 'read-only'):
                if mType.startswith('std::weak_ptr<'):
                    code += dedentString("""
                    bool {0}::set{2}(const {3}& {1})
                    {{
                      auto mlocked = m_{1}.lock();
                      auto vlocked = {1}.lock();
                      if (mlocked == vlocked)
                      {{
                        return false;
                      }}
                      m_{1} = vlocked;
                      return true;
                    }}
                    """.format(bareName, memberName, camelCase(memberName), mType)) + '\n'
                elif 'access' in memberDef and memberDef['access'] == 'force-write':
                    # A set method that always overwrites previous values and returns true.
                    code += dedentString("""
                    bool {0}::set{2}(const {3}& {1})
                    {{
                      m_{1} = {1};
                      return true;
                    }}
                    """.format(bareName, memberName, camelCase(memberName), mType)) + '\n'
                else:
                    code += dedentString("""
                    bool {0}::set{2}(const {3}& {1})
                    {{
                      if (m_{1} == {1})
                      {{
                        return false;
                      }}
                      m_{1} = {1};
                      return true;
                    }}
                    """.format(bareName, memberName, camelCase(memberName), mType)) + '\n'

            # Add "get" methods
            if not explicitGetter and (memberDef['access'] != 'write-only' if 'access' in memberDef else True):
                code += dedentString("""
                const {3}& {0}::{1}() const
                {{
                  return m_{1};
                }}

                {3}& {0}::{1}()
                {{
                  return m_{1};
                }}
                """.format(bareName, memberName, camelCase(memberName), mType)) + '\n'
    return code

def isPureVirtualMethod(method):
    if 'attributes' in method:
        if 'pure virtual' in method['attributes']:
            return True
    return False

def hasPureVirtualMethods(methods):
    for method in methods:
        if isPureVirtualMethod(method):
            return True
    return False

def checkAbstract(classDict):
    if 'methods' in classDict:
        if 'methods' in classDict and hasPureVirtualMethods(classDict['methods']):
            return True
        if 'protected-methods' in classDict and hasPureVirtualMethods(classDict['protected-methods']):
            return True
    return False

def emitClass(name, classType, classList, inherit, moduleData):
    ns, bareName = bareClassName(name)
    superclassNamespace, superclassName = bareClassName(inherit[-1]) if len(inherit) > 0 else ([], '')
    print('--- %s ---' % bareName)

    # Header body
    entry = classList[name]
    if type(entry) is not dict:
        return
    isAbstract = checkAbstract(entry)
    extraIncludes = entry['header-includes'] if 'header-includes'  in entry else []
    if 'arcs' in entry:
        if len([x for x in entry['arcs'] if entry['arcs'][x]['template'] != 'Arc']) > 0:
            extraIncludes += ['smtk/markup/ArcEditor.h',]
    headerData = generateHeaderHeader(
            ns, bareName, inherit,
            extraIncludes,
            entry['header-forward-classes'] if 'header-forward-classes'  in entry else [])

    # Doxygen documentation
    if 'description' in entry:
        # print('  Add docs')
        description = dedentString(entry['description']).split('\n')
        for line in description:
            headerData += '/// {0}\n'.format(line)
    # Class statement and type macro
    if inherit is not None and len(inherit) > 0:
        headerData += dedentString("""
        class {exportMacro} {0} : public {2}
        {{
        public:
          smtkTypeMacro({1});
          smtkSuperclassMacro({2});
        """.format(bareName, name, inherit[-1], exportMacro=exportMacro(name)));
        # print('Declare %s : public %s' % (name, inherit))
    else:
        headerData += dedentString("""
        class {exportMacro} {0} : smtkEnableSharedPtr({0})
        {{
        public:
          smtkTypeMacroBase({1});
        """.format(bareName, name, exportMacro=exportMacro(name)));
        # print('Declare %s' % name)

    # Do not use smtkCreateMacro for now.
    # if not isAbstract:
    #     headerData += '  smtkCreateMacro({0});\n'.format(inherit[0] if len(inherit) > 0 else name)
    nextInherit = inherit + [name]

    # Enums and type aliases
    if 'enums' in entry:
        headerData += '\n';
        # print('  Add ¨enum¨ statements')
        enums = entry['enums']
        for enumName in enums.keys():
            headerData += '  enum {0}\n{{ {1} }};\n'.format(enumName, ','.join(enums[enumName].keys()))

    if 'type-aliases' in entry:
        headerData += '\n';
        # print('  Add ¨using¨ statements')
        aliases = entry['type-aliases']
        for alias in aliases:
            headerData += '  using {0} = {1};\n'.format(alias, aliases[alias])

    # Declare contstructor and destructor
    if not isAbstract:
        headerData += indentString(dedentString("""

            template<typename... Args>
            {0}(Args&&... args)
              : {1}::{2}(std::forward<Args>(args)...)
            {{
            }}

            ~{0}() override;
        """.format(bareName, '::'.join(superclassNamespace), superclassName)), 2)

    # Add method signatures
    methodNames = set()
    if 'methods' in entry:
        headerData += '\n';
        methods = entry['methods']
        for methodName, method in methods.items():
            if 'description' in method:
                description = dedentString(method['description']).split('\n')
                for line in description:
                    headerData += '  /// {0}\n'.format(line)
            headerData += '  {0};\n'.format(method['signature'])
            methodNames.add(methodName)
    # Add accessors for member variables.
    if 'members' in entry:
        members = entry['members']
        for memberName, member in members.items():
            access = member['access'] if 'access' in member else 'read-write'
            attributes = member['attributes'] if 'attributes' in member else {}
            override = ' override' if 'override' in attributes and attributes['override'] else ''
            casedName = camelCase(memberName)
            headerData += '\n';
            if 'description' in member:
                description = dedentString(member['description']).split('\n')
                for line in description:
                    headerData += '  /// {0}\n'.format(line)
            if access == 'public':
                # Do not provide setter/getter methods, instead declare here.
                headerData += '  {0} {1};'.format(member['type'], memberName)
                continue # Skip setter/getter generation
            # Do not declare a set-method if read-only or explicitly declared.
            if (access == 'read-write' or access == 'force-write') and 'set{0}'.format(camelCase(memberName)) not in methodNames:
                headerData += '  bool set{0}(const {1}& {2}){3};\n'.format(casedName, member['type'], memberName, override)
            # Do not declare a get-method if explicitly declared.
            if access != 'write-only' and memberName not in methodNames:
                headerData += '  const {0}& {1}() const{2};\n'.format(member['type'], memberName, override)
                headerData += '  {0}& {1}(){2};\n'.format(member['type'], memberName, override)
    # Add accessors for arcs that originate from instances of this class.
    if 'arcs' in entry:
        arcs = entry['arcs']
        for arcName, arcData in arcs.items():
            if arcName in methodNames:
                # Skip arcs that have a manually-provided implementation.
                continue
            if arcData['template'] == 'Arcs':
                # Provide access to multiple, unsorted nodes of dest-type
                headerData += indentString(dedentString("""

                    ArcEditor<SelfType, {1}> {0}() const;
                """.format(arcName, arcData['dest-type'])), 2)
            elif arcData['template'] == 'Arc':
                # Provide access to a single node of dest-type
                headerData += indentString(dedentString("""

                    const {1}& {0}() const;
                    {1}& {0}();
                    bool set{0}(const {1}&);
                """.format(arcName, arcData['dest-type'])), 2)
            else:
                print('ERROR: Unhandled arc template %s' % arcData['template'])
    headerData += '\nprotected:\n'
    if 'internal-classes' in entry:
        for internalClassName, internalClass in entry['internal-classes'].items():
            description = dedentString(internalClass['description']).split('\n') if 'description' in internalClass else []
            if len(description):
                for line in description:
                    headerData += '  /// {0}\n'.format(line)
            headerData += '  {0} {1};\n'.format(internalClass['type'], internalClassName)
        headerData += '\n'
    haveProtectedMethods = False
    if 'protected-methods' in entry:
        haveProtectedMethods = True
        methods = entry['protected-methods']
        for method in methods:
            headerData += '  {0};\n'.format(methods[method]['signature'])
        # print('  Add methods')
    if 'members' in entry:
        if haveProtectedMethods:
            headerData += '\n'
        # print('  Add members')
        members = entry['members']
        for memberName, member in members.items():
            if 'access' in member and member['access'] == 'public':
                continue
            headerData += '  {0} m_{1};\n'.format(member['type'], memberName)

    headerData += '};\n'
    headerData += generateHeaderFooter(ns, bareName)

    # Source body
    sourceData = generateSourceHeader(ns, bareName)
    includes = set()
    iclss = generateInternalClasses(entry)
    cdtor = generateCtorDtor(ns, bareName, entry, includes)
    mstub = generateMethodStubs(ns, bareName, entry, includes)
    allIncludes = []
    if 'header-forward-classes' in entry:
        allIncludes += [classNameToHeaderName(x) for x in entry['header-forward-classes']]
    if 'source-includes' in entry:
        allIncludes += [x for x in entry['source-includes']]
    allIncludes += list(includes)
    sourceData += includeDirectives(allIncludes)
    sourceData += openNamespaces(ns)
    sourceData += iclss
    sourceData += cdtor
    sourceData += mstub
    sourceData += generateSourceFooter(ns, bareName)
    # Skip the first entry in the inheritance tree for node classes.
    if classType == 'arcs' or (len(inherit) > 0 and name is not inherit[0]):
        if classType == 'arcs':
            tns = tuple(ns + ['arcs'])
        else:
            tns = tuple(ns)
        if tns not in moduleData:
            moduleData[tns] = { 'classes': [] }
        moduleData[tns]['classes'] += [bareName]

        os.makedirs(os.path.join(*tns), exist_ok=True)
        if writeFileIfDifferent(headerData, os.path.join(*tns, '%s.h' % bareName)):
            print('    header updated')
        if writeFileIfDifferent(sourceData, os.path.join(*tns, '%s.cxx' % bareName)):
            print('    source updated')
        # header = open(os.path.join(*tns, '%s.h' % bareName), 'w')
        # header.write(headerData)
        # source = open(os.path.join(*tns, '%s.cxx' % bareName), 'w')
        # source.write(sourceData)
        # print(headerData)
    generate(classList[name], classType, moduleData, nextInherit)

def generate(classList, classType = 'nodes', moduleData = {}, inherit = None):
    if inherit is None:
        inherit = [] if classType == 'arcs' else ['smtk::graph::Component']

    keywords = set(['access', 'arcs', 'attributes', 'body', 'description', 'dest-type', 'enums', 'header-includes', 'header-forward-classes', 'internal-classes', 'inverse', 'members', 'methods', 'protected-methods', 'signature', 'source-includes', 'source-type', 'template', 'type-aliases'])
    for name in classList.keys():
        if name in keywords:
            continue

        emitClass(name, classType, classList, inherit, moduleData)
        # emitJSONClass(name, classType, classList, inherit, moduleData)

def librarySources(libraryName, libraryData, moduleData, source):
    sourceList = []
    libraryPath = os.path.join(*libraryData)
    for path, config in moduleData.items():
        basePath = os.path.join(*path)
        if basePath.find(libraryPath) == 0:
            if source in config:
                relPath = basePath[len(libraryPath)+1:]
                print('base "%s" lib "%s" rel "%s"' % (basePath, libraryPath, relPath))
                for sourceFile in config[source]:
                    sourceList += [os.path.join(relPath, sourceFile)]
    return sourceList

def buildFiles(moduleData, libraries):
    for libraryName, libraryData in libraries.items():
        cmf = os.path.join(*libraryData, 'CMakeLists.txt')
        cmake = open(cmf, 'w')
        cmakeData = dedentString("""
          set(sources)
          set(headers)
          """)

        for source in ['headers', 'sources', 'classes']:
            sourceList = librarySources(libraryName, libraryData, moduleData, source)
            if len(sourceList) > 0:
                cmakeData += '\nlist(APPEND {0}\n'.format(source)
                for sourceFile in sourceList:
                    cmakeData += '  {0}\n'.format(sourceFile)
                cmakeData += ')\n'

        cmakeData += dedentString("""

        # Turn "classes" into source and headers entries
        foreach(class ${{classes}})
          list(APPEND headers ${{class}}.h)
          list(APPEND sources ${{class}}.cxx)
        endforeach()

        add_library({0} SHARED ${{sources}} ${{headers}})
        add_dependencies({0} ${{aevaDependencies}})
        target_link_libraries({0}
          PUBLIC
            smtkCore
            smtkPVServerExt
            vtkSMTKSourceExt
            VTK::AEVAExt
            VTK::MeshingNetGen
            nglib
          PRIVATE
            VTK::CommonCore
            VTK::CommonDataModel
            VTK::IOImage
            VTK::IOXML
            VTK::IOGeometry
            VTK::FiltersCore
            VTK::FiltersExtraction
            VTK::FiltersGeometry
            VTK::FiltersParallelDIY2
            VTK::FiltersPoints
            smtkPQComponentsExt
            ${{ITK_LIBRARIES}}
            ${{__dependencies}}
            Boost::filesystem
            VTK::hdf5
        )
        """.format(libraryName))
        print(cmakeData, file=cmake)

moduleData = {}
generate(nodeClasses, 'nodes', moduleData)
generate(arcClasses, 'arcs', moduleData)
# print(str(moduleData))
#buildFiles(moduleData, {'smtkMarkup': ('smtk', 'markup')})
