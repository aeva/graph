################################################
aeva: Annotation and Exchange of Virtual Anatomy
################################################
Version: |release|

.. highlight:: c++

.. role:: cxx(code)
   :language: c++

.. findimage:: aeva-logo.*

Contents:

.. toctree::
   :maxdepth: 4

   developer/index.rst
   tutorials/index.rst


##################
Indices and tables
##################

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
