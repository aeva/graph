-----------------------------------
Obtaining, Building, and Installing
-----------------------------------

For instructions on obtaining, building, and installing
aeva, clone the repository:

.. code:: sh

   git clone https://gitlab.kitware.com/aeva/graph.git

and follow the instructions in the :file:`ReadMe.md` file
in the top-level source directory.
The rest of this user's guide assumes you have built
and installed aeva according to these instructions.

In addition to cloning anonymously as mentioned above,
you are also welcome to create an account on either
gitlab.kitware.com or github.com and fork the repository.
Forking the repository will allow you to submit contributions
back to aeva for inclusion in later releases.
See :ref:`aeva-contributing` for more on how to
prepare and submit changes to aeva.
