.. _aeva-contributing:

--------------------
Contributing to aeva
--------------------

.. role:: cxx(code)
   :language: c++

.. role:: cmake(code)
   :language: cmake

.. contents::

The first step to contributing to aeva is to obtain the source code and build it.
The top-level ReadMe.md file in the source code includes instructions for building aeva.
The rest of this section discusses how the source and documentation are organized
and provides guidelines for how to match the aeva style.

.. toctree::
   :maxdepth: 3

   organization.rst
   extending.rst
   exposing.rst
   style.rst
   documentation.rst
   testing.rst
   todo.rst
