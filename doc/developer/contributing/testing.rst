Testing
=======

Testing is important to keep aeva functioning as development continues.
All new functionality added to aeva should include tests.
When you are preparing to write tests, consider the following

* Unit tests should be present to provide coverage of new classes and methods.
* Build-failure tests provide coverage for template metaprogramming by
  attempting to build code that is expected to cause static assertions or
  other compilation failures.
* Integration tests should be present to ensure features work in combination
  with one another as intended; these tests should model how users are expected
  to exercise aeva in a typical workflow.
* Regression tests should be added when users discover a problem in functionality
  not previously tested and which further development may reintroduce.
* Contract tests should be added for downstream projects (i.e., those which depend
  on aeva) which aeva should not break through API or behavioral changes.
  A contract test works by cloning, building, and testing an external project;
  if the external project's tests all succeed, then the contract test succeeds.
  Otherwise, the contract test fails.

Unit tests
----------

aeva uses a CMake macro from SMTK named ``smtk_unit_tests`` that you should use to create unit tests.
This macro will create a single executable that runs tests in multiple source files;
this reduces the number of executables in aeva and makes tests more uniform.
Because there is a single executable, you should make your test a function whose name
matches the name of your source file (e.g., ``int TestResource(int, const char* [])``)
rather than ``int main(int, const char* [])``.
The CMake macro also allows a LABEL to be assigned to each of the tests in the executable;
this label can be used during development to run a subset of tests and during integration
to identify areas related to test failures or timings in CDash.
