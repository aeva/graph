Source code organization
========================

To a first approximation, aeva's directory structure mirrors the namespaces used:
classes in the :cxx:`smtk::session::aeva` namespace are mostly found in the
:file:`smtk/session/aeva` directory.
Exceptions occur where classes that belong in a namespace depend on third-party libraries
that should not be linked to aeva's core library.

Inside :file:`smtk/`, subdirectories, there are :file:`testing/` directories that
hold :file:`python/` and :file:`cxx/` directories for Python and C++ tests, respectively.
