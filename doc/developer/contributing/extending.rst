Extending aeva
==============

As with all software, it is important to understand where
functionality you wish to add belongs: in your project,
in aeva's core library, or in an upstream dependency of aeva.

The tutorials provide in-depth guides on how to extend aeva
in certain obvious directions,

* Writing an attribute resource template file to accept standardized annotation.
* Writing an exporter to support a new solver's input format.
* Adding a new operation

These tasks are all examples of projects that might use aeva
as a dependency but not alter aeva itself.
On the other hand, if you are attempting to provide functionality
that (a) does not introduce dependencies on new third-party libraries;
(b) will be useful to most projects that use aeva; and
(c) cannot be easily be factored into a separate package,
then it may be best to contribute these changes to aeva itself.
In that case, the rest of this section discusses how aeva should be
modified and changes submitted for consideration.
