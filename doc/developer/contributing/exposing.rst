Exposing aeva for use in external projects
==========================================

aeva generates a file named :file:`aeva-sessionConfig.cmake` that allows other projects to find and use aeva.
This file is installed to :file:`${CMAKE_INSTALL_PREFIX}/lib/cmake/aeva-session/`.
External projects can add aeva with

.. code:: cmake

    find_package(aeva-session)

Then, when building external projects, set CMake's :cmake:`aeva-session_DIR` to the directory containing :file:`aeva-sessionConfig.cmake`.
Note that you may point external projects to the top level of an aeva build directory or
an install tree's :file:`lib/cmake/aeva-session` directory; both contain an :file:`aeva-sessionConfig.cmake` file
suitable for use by external projects.
The former is suitable for development, since you may be modifying both aeva and a project
that depends on it — having to re-install aeva after each change becomes tedious.
The latter is suitable for creating packages and distributing software.

If you add a new dependency to aeva, :file:`CMake/aeva-sessionConfig.cmake.in` (which is used to create
:file:`aeva-sessionConfig.cmake`) should be configured to find the dependent package so that consumers
of aeva have access to it without additional work.

If you add a new option to aeva, it should be exposed in :file:`cmake/Options.h.in`.
