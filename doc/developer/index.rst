======================
aeva Developer's Guide
======================

aeva is a framework for Annotation and Exchange of Virtual Anatomy.
It is built on top of the `Simulation Modeling Tool Kit (SMTK)`_
and this guide assumes you have a basic knowledge of SMTK.

.. toctree::
   :maxdepth: 4

   obtain-build-install.rst
   overview.rst
   data-model.rst


.. _Simulation Modeling Tool Kit (SMTK): https://smtk.readthedocs.io/
