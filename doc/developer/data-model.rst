.. _aeva-data-model:

-----------------
aeva's Data Model
-----------------

aeva stores discrete models as VTK data objects and parametric models using OpenCASCADE.
All aeva components (discrete and parametric) with a visual representation provide
VTK data via SMTK's VTK geometry backend.
In the case of discrete models, this visual representation is usually just a copy of the
data object used to model the component.
In the case of parametric models, the VTK data is a render-quality VTK mesh of the CAD model
produced using OpenCASCADE's tessellation libraries.

Outline
=======

* Primary and reference geometry.
    * Global IDs are "owned" by primary geometry and "used" by reference geometry (to refer back to the primary)
    * Primary geometry may have both global IDs and pedigree IDs.
      The latter relate primary geometry to its immediate ancestor (other primary geometry).
* Data types
    * Image data
    * Unstructured data (volumetric and/or surface data)
    * Polygonal data (surface data only)
* Annotations
    * Templated attributes (smtk::attribute::Attribute)
    * Free-form attributes (actually smtk::resource::Properties data)
