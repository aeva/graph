.. _smtk-overview:

-----------------------------
An Overview of aeva Resources
-----------------------------

aeva uses SMTK's graph resource to model data and annotations
relevant to human anatomy.
In aeva, data and annotations are represented as nodes in a graph.
Arcs connect the nodes in the graph and indicate the nature of the
relationship between the nodes.

* **Annotation** is the basic type for all annotation nodes.
  Annotations may be geometric (i.e., a locus of points in space)
  or informatic (i.e., functional or categorical data that has no
  spatial extent) in nature.
