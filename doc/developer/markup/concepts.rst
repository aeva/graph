Markup Resources
----------------

The "markup" subsystem is an SMTK :smtk:`resource <smtk::markup::Resource>`
and :smtk:`components <smtk::markup::Component>` that facilitate the
annotation of geometric data.
The way in which geometry and annotations are represented
is what determines the capabilities of the resulting application.

The key concept this resource embodies is a set-theoretic approach
to referencing geometry for annotation.
The resource itself owns a catalog of :smtk:`domains <smtk::markup::Domain>`,
each of which provides a way to enumerate its members and reference sets
of members (and in some cases, the geometric boundaries of those members).
Annotations then combine a reference to domain members with
information relevant to the geometric context of the reference.

For example, simulations often annotate subregions of their domain
with material properties; boundaries of these regions with
boundary conditions; and other miscellaneous information
including initial conditions, termination conditions, and triggers for
checkpoints or post-processing.

Another example is medical studies, where annotations on
radiographic information relate regions to tissues, organs,
and landmarks of interest.
These annotations are used for morphological analysis in
cross-sectional and longitudinal studies in order to
characterize populations, diagnose patients, plan surgical
procedures, etc.

There are two types of domains: discrete and parametric.
Discrete domains have members represented by integer identifiers
which live in an :smtk:`"ID space" <smtk::markup::IdSpace>`.
Parametric domains have members represented by continuous ranges
which live in a :smtk:`"parameter space" <smtk::markup::ParameterSpace>`.
The markup subsystem is currently focused on discrete domains
although some capabilities exist for parametric domains.

Discrete domains
================

Discrete domains are composed of primitive shapes
(points, lines, triangles, quadrilaterals, tetrahedra,
hexahedra, etc.) that are defined by corner points.
The points have unique IDs assigned to them as does
each primitive, resulting in two ID spaces: points
and cells.

Parametric domains
==================

Because parametric domains are continuous,
they are difficult to represent computationally
since real numbers are only approximately
represented by digital computers.

In the markup resource, parameter spaces are
represented by mapping discrete spaces through
a continuous function (i.e., the map from
parameter space to world coordinates is
continuous but the domain of that map is
discretized).
