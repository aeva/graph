//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "vtk/aeva/ext/vtkProportionalEditWidget.h"
#include "vtk/aeva/ext/vtkProportionalEditRepresentation.h"

#include "vtkCallbackCommand.h"
#include "vtkCamera.h"
#include "vtkCommand.h"
#include "vtkEvent.h"
#include "vtkObjectFactory.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkStdString.h"
#include "vtkWidgetCallbackMapper.h"
#include "vtkWidgetEvent.h"
#include "vtkWidgetEventTranslator.h"

vtkStandardNewMacro(vtkProportionalEditWidget);

//----------------------------------------------------------------------------
vtkProportionalEditWidget::vtkProportionalEditWidget()
{
  this->WidgetState = vtkProportionalEditWidget::Start;

  // Define widget events
  this->CallbackMapper->SetCallbackMethod(vtkCommand::LeftButtonPressEvent,
    vtkWidgetEvent::Select,
    this,
    vtkProportionalEditWidget::SelectAction);
  this->CallbackMapper->SetCallbackMethod(vtkCommand::LeftButtonReleaseEvent,
    vtkWidgetEvent::EndSelect,
    this,
    vtkProportionalEditWidget::EndSelectAction);
  this->CallbackMapper->SetCallbackMethod(vtkCommand::MiddleButtonPressEvent,
    vtkWidgetEvent::Translate,
    this,
    vtkProportionalEditWidget::TranslateAction);
  this->CallbackMapper->SetCallbackMethod(vtkCommand::MiddleButtonReleaseEvent,
    vtkWidgetEvent::EndTranslate,
    this,
    vtkProportionalEditWidget::EndSelectAction);
  this->CallbackMapper->SetCallbackMethod(vtkCommand::RightButtonPressEvent,
    vtkWidgetEvent::Scale,
    this,
    vtkProportionalEditWidget::ScaleAction);
  this->CallbackMapper->SetCallbackMethod(vtkCommand::RightButtonReleaseEvent,
    vtkWidgetEvent::EndScale,
    this,
    vtkProportionalEditWidget::EndSelectAction);
  this->CallbackMapper->SetCallbackMethod(
    vtkCommand::MouseMoveEvent, vtkWidgetEvent::Move, this, vtkProportionalEditWidget::MoveAction);
  this->CallbackMapper->SetCallbackMethod(vtkCommand::KeyPressEvent,
    vtkEvent::AnyModifier,
    30,
    1,
    "Up",
    vtkWidgetEvent::Up,
    this,
    vtkProportionalEditWidget::MoveConeAction);
  this->CallbackMapper->SetCallbackMethod(vtkCommand::KeyPressEvent,
    vtkEvent::AnyModifier,
    28,
    1,
    "Right",
    vtkWidgetEvent::Up,
    this,
    vtkProportionalEditWidget::MoveConeAction);
  this->CallbackMapper->SetCallbackMethod(vtkCommand::KeyPressEvent,
    vtkEvent::AnyModifier,
    31,
    1,
    "Down",
    vtkWidgetEvent::Down,
    this,
    vtkProportionalEditWidget::MoveConeAction);
  this->CallbackMapper->SetCallbackMethod(vtkCommand::KeyPressEvent,
    vtkEvent::AnyModifier,
    29,
    1,
    "Left",
    vtkWidgetEvent::Down,
    this,
    vtkProportionalEditWidget::MoveConeAction);
}

//----------------------------------------------------------------------------
vtkProportionalEditWidget::~vtkProportionalEditWidget() = default;

//----------------------------------------------------------------------
void vtkProportionalEditWidget::SelectAction(vtkAbstractWidget* w)
{
  auto* self = reinterpret_cast<vtkProportionalEditWidget*>(w);

  // Get the event position
  int X = self->Interactor->GetEventPosition()[0];
  int Y = self->Interactor->GetEventPosition()[1];

  // We want to update the radius, axis and center as appropriate
  reinterpret_cast<vtkProportionalEditRepresentation*>(self->WidgetRep)
    ->SetInteractionState(vtkProportionalEditRepresentation::Moving);
  int interactionState = self->WidgetRep->ComputeInteractionState(X, Y);
  self->UpdateCursorShape(interactionState);

  if (self->WidgetRep->GetInteractionState() == vtkProportionalEditRepresentation::Outside)
  {
    return;
  }

  if (self->Interactor->GetControlKey() &&
    interactionState == vtkProportionalEditRepresentation::MovingWhole)
  {
    reinterpret_cast<vtkProportionalEditRepresentation*>(self->WidgetRep)
      ->SetInteractionState(vtkProportionalEditRepresentation::TranslatingCenter);
  }

  // We are definitely selected
  self->GrabFocus(self->EventCallbackCommand);
  double eventPos[2];
  eventPos[0] = static_cast<double>(X);
  eventPos[1] = static_cast<double>(Y);
  self->WidgetState = vtkProportionalEditWidget::Active;
  self->WidgetRep->StartWidgetInteraction(eventPos);

  self->EventCallbackCommand->SetAbortFlag(1);
  self->StartInteraction();
  self->InvokeEvent(vtkCommand::StartInteractionEvent, nullptr);
  self->Render();
}

//----------------------------------------------------------------------
void vtkProportionalEditWidget::TranslateAction(vtkAbstractWidget* w)
{
  auto* self = reinterpret_cast<vtkProportionalEditWidget*>(w);

  // Get the event position
  int X = self->Interactor->GetEventPosition()[0];
  int Y = self->Interactor->GetEventPosition()[1];

  // We want to compute an orthogonal vector to the pane that has been selected
  reinterpret_cast<vtkProportionalEditRepresentation*>(self->WidgetRep)
    ->SetInteractionState(vtkProportionalEditRepresentation::Moving);
  int interactionState = self->WidgetRep->ComputeInteractionState(X, Y);
  self->UpdateCursorShape(interactionState);

  if (self->WidgetRep->GetInteractionState() == vtkProportionalEditRepresentation::Outside)
  {
    return;
  }

  // We are definitely selected
  self->GrabFocus(self->EventCallbackCommand);
  double eventPos[2];
  eventPos[0] = static_cast<double>(X);
  eventPos[1] = static_cast<double>(Y);
  self->WidgetState = vtkProportionalEditWidget::Active;
  self->WidgetRep->StartWidgetInteraction(eventPos);

  self->EventCallbackCommand->SetAbortFlag(1);
  self->StartInteraction();
  self->InvokeEvent(vtkCommand::StartInteractionEvent, nullptr);
  self->Render();
}

//----------------------------------------------------------------------
void vtkProportionalEditWidget::ScaleAction(vtkAbstractWidget* w)
{
  auto* self = reinterpret_cast<vtkProportionalEditWidget*>(w);

  // Get the event position
  int X = self->Interactor->GetEventPosition()[0];
  int Y = self->Interactor->GetEventPosition()[1];

  // We want to compute an orthogonal vector to the pane that has been selected
  reinterpret_cast<vtkProportionalEditRepresentation*>(self->WidgetRep)
    ->SetInteractionState(vtkProportionalEditRepresentation::Scaling);
  int interactionState = self->WidgetRep->ComputeInteractionState(X, Y);
  self->UpdateCursorShape(interactionState);

  if (self->WidgetRep->GetInteractionState() == vtkProportionalEditRepresentation::Outside)
  {
    return;
  }

  // We are definitely selected
  self->GrabFocus(self->EventCallbackCommand);
  double eventPos[2];
  eventPos[0] = static_cast<double>(X);
  eventPos[1] = static_cast<double>(Y);
  self->WidgetState = vtkProportionalEditWidget::Active;
  self->WidgetRep->StartWidgetInteraction(eventPos);

  self->EventCallbackCommand->SetAbortFlag(1);
  self->StartInteraction();
  self->InvokeEvent(vtkCommand::StartInteractionEvent, nullptr);
  self->Render();
}

//----------------------------------------------------------------------
void vtkProportionalEditWidget::MoveAction(vtkAbstractWidget* w)
{
  auto* self = reinterpret_cast<vtkProportionalEditWidget*>(w);

  // So as to change the cursor shape when the mouse is poised over
  // the widget. Unfortunately, this results in a few extra picks
  // due to the cell picker. However given that its picking simple geometry
  // like the handles/arrows, this should be very quick
  int X = self->Interactor->GetEventPosition()[0];
  int Y = self->Interactor->GetEventPosition()[1];
  int changed = 0;

  if (self->ManagesCursor && self->WidgetState != vtkProportionalEditWidget::Active)
  {
    int oldInteractionState =
      reinterpret_cast<vtkProportionalEditRepresentation*>(self->WidgetRep)->GetInteractionState();

    reinterpret_cast<vtkProportionalEditRepresentation*>(self->WidgetRep)
      ->SetInteractionState(vtkProportionalEditRepresentation::Moving);
    int state = self->WidgetRep->ComputeInteractionState(X, Y);
    changed = self->UpdateCursorShape(state);
    reinterpret_cast<vtkProportionalEditRepresentation*>(self->WidgetRep)
      ->SetInteractionState(oldInteractionState);
    changed = (changed || state != oldInteractionState) ? 1 : 0;
  }

  // See whether we're active
  if (self->WidgetState == vtkProportionalEditWidget::Start)
  {
    if (changed && self->ManagesCursor)
    {
      self->Render();
    }
    return;
  }

  // Okay, adjust the representation
  double e[2];
  e[0] = static_cast<double>(X);
  e[1] = static_cast<double>(Y);
  self->WidgetRep->WidgetInteraction(e);

  // moving something
  self->EventCallbackCommand->SetAbortFlag(1);
  self->InvokeEvent(vtkCommand::InteractionEvent, nullptr);
  self->Render();
}

//----------------------------------------------------------------------
void vtkProportionalEditWidget::EndSelectAction(vtkAbstractWidget* w)
{
  auto* self = reinterpret_cast<vtkProportionalEditWidget*>(w);

  if (self->WidgetState != vtkProportionalEditWidget::Active ||
    self->WidgetRep->GetInteractionState() == vtkProportionalEditRepresentation::Outside)
  {
    return;
  }

  // Return state to not selected
  double e[2];
  self->WidgetRep->EndWidgetInteraction(e);
  self->WidgetState = vtkProportionalEditWidget::Start;
  self->ReleaseFocus();

  // Update cursor if managed
  self->UpdateCursorShape(reinterpret_cast<vtkProportionalEditRepresentation*>(self->WidgetRep)
                            ->GetRepresentationState());

  self->EventCallbackCommand->SetAbortFlag(1);
  self->EndInteraction();
  self->InvokeEvent(vtkCommand::EndInteractionEvent, nullptr);
  self->Render();
}

//----------------------------------------------------------------------
void vtkProportionalEditWidget::MoveConeAction(vtkAbstractWidget* w)
{
  auto* self = reinterpret_cast<vtkProportionalEditWidget*>(w);

  reinterpret_cast<vtkProportionalEditRepresentation*>(self->WidgetRep)
    ->SetInteractionState(vtkProportionalEditRepresentation::Moving);

  int X = self->Interactor->GetEventPosition()[0];
  int Y = self->Interactor->GetEventPosition()[1];
  self->WidgetRep->ComputeInteractionState(X, Y);

  // The cursor must be over part of the widget for these key presses to work
  if (self->WidgetRep->GetInteractionState() == vtkProportionalEditRepresentation::Outside)
  {
    return;
  }

  // Invoke all of the events associated with moving the cylinder
  self->InvokeEvent(vtkCommand::StartInteractionEvent, nullptr);

  // Move the cylinder
  double factor = (self->Interactor->GetControlKey() ? 0.5 : 1.0);
  if (vtkStdString(self->Interactor->GetKeySym()) == vtkStdString("Down") ||
    vtkStdString(self->Interactor->GetKeySym()) == vtkStdString("Left"))
  {
    self->GetProportionalEditRepresentation()->BumpCone(-1, factor);
  }
  else
  {
    self->GetProportionalEditRepresentation()->BumpCone(1, factor);
  }
  self->InvokeEvent(vtkCommand::InteractionEvent, nullptr);

  self->EventCallbackCommand->SetAbortFlag(1);
  self->InvokeEvent(vtkCommand::EndInteractionEvent, nullptr);
  self->Render();
}

//----------------------------------------------------------------------
void vtkProportionalEditWidget::SetEnabled(int enabling)
{
  if (this->Enabled == enabling)
  {
    return;
  }

  Superclass::SetEnabled(enabling);
}

//----------------------------------------------------------------------
void vtkProportionalEditWidget::CreateDefaultRepresentation()
{
  if (!this->WidgetRep)
  {
    this->WidgetRep = vtkProportionalEditRepresentation::New();
  }
}

//----------------------------------------------------------------------
void vtkProportionalEditWidget::SetRepresentation(vtkProportionalEditRepresentation* rep)
{
  this->Superclass::SetWidgetRepresentation(reinterpret_cast<vtkWidgetRepresentation*>(rep));
}

//----------------------------------------------------------------------
int vtkProportionalEditWidget::UpdateCursorShape(int interactionState)
{
  // So as to change the cursor shape when the mouse is poised over
  // the widget.
  if (this->ManagesCursor)
  {
    if (interactionState == vtkProportionalEditRepresentation::Outside)
    {
      return this->RequestCursorShape(VTK_CURSOR_DEFAULT);
    }
    if (interactionState == vtkProportionalEditRepresentation::AdjustingRadius)
    {
      return this->RequestCursorShape(VTK_CURSOR_SIZEALL);
    }
    return this->RequestCursorShape(VTK_CURSOR_HAND);
  }

  return 0;
}

//----------------------------------------------------------------------------
void vtkProportionalEditWidget::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
