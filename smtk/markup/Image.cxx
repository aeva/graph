// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/Image.h"

#include "vtkImageData.h"

namespace smtk
{
namespace markup
{

Image::~Image() = default;

void Image::initialize(const nlohmann::json& data, json::Helper& helper)
{
}

bool Image::setAxes(const std::vector<std::weak_ptr<smtk::markup::AssignedIds>>& axes)
{
  m_axes = axes;
  return true;
}

const std::vector<std::weak_ptr<smtk::markup::AssignedIds>>& Image::axes() const
{
  return m_axes;
}

std::vector<std::weak_ptr<smtk::markup::AssignedIds>>& Image::axes()
{
  return m_axes;
}

bool Image::setPoints(const std::weak_ptr<smtk::markup::AssignedIds>& points)
{
  auto mlocked = m_points.lock();
  auto vlocked = points.lock();
  if (mlocked == vlocked)
  {
    return false;
  }
  m_points = vlocked;
  return true;
}

const std::weak_ptr<smtk::markup::AssignedIds>& Image::points() const
{
  return m_points;
}

std::weak_ptr<smtk::markup::AssignedIds>& Image::points()
{
  return m_points;
}

bool Image::setCells(const std::weak_ptr<smtk::markup::AssignedIds>& cells)
{
  auto mlocked = m_cells.lock();
  auto vlocked = cells.lock();
  if (mlocked == vlocked)
  {
    return false;
  }
  m_cells = vlocked;
  return true;
}

const std::weak_ptr<smtk::markup::AssignedIds>& Image::cells() const
{
  return m_cells;
}

std::weak_ptr<smtk::markup::AssignedIds>& Image::cells()
{
  return m_cells;
}

bool Image::setData(const vtkSmartPointer<vtkImageData>& data)
{
  if (m_data == data)
  {
    return false;
  }
  m_data = data;
  return true;
}

const vtkSmartPointer<vtkImageData>& Image::data() const
{
  return m_data;
}

vtkSmartPointer<vtkImageData>& Image::data()
{
  return m_data;
}


} // namespace markup
} // namespace smtk
