// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/Subset.h"

namespace smtk
{
namespace markup
{

Subset::~Subset() = default;

void Subset::initialize(const nlohmann::json& data, json::Helper& helper)
{
}

bool Subset::setIds(const std::weak_ptr<smtk::markup::AssignedIds>& ids)
{
  auto mlocked = m_ids.lock();
  auto vlocked = ids.lock();
  if (mlocked == vlocked)
  {
    return false;
  }
  m_ids = vlocked;
  return true;
}

const std::weak_ptr<smtk::markup::AssignedIds>& Subset::ids() const
{
  return m_ids;
}

std::weak_ptr<smtk::markup::AssignedIds>& Subset::ids()
{
  return m_ids;
}


} // namespace markup
} // namespace smtk
