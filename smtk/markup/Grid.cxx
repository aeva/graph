// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/Grid.h"

namespace smtk
{
namespace markup
{

Grid::~Grid() = default;

void Grid::initialize(const nlohmann::json& data, json::Helper& helper)
{
}

bool Grid::setPoints(const std::weak_ptr<smtk::markup::AssignedIds>& points)
{
  auto mlocked = m_points.lock();
  auto vlocked = points.lock();
  if (mlocked == vlocked)
  {
    return false;
  }
  m_points = vlocked;
  return true;
}

const std::weak_ptr<smtk::markup::AssignedIds>& Grid::points() const
{
  return m_points;
}

std::weak_ptr<smtk::markup::AssignedIds>& Grid::points()
{
  return m_points;
}

bool Grid::setCells(const std::weak_ptr<smtk::markup::AssignedIds>& cells)
{
  auto mlocked = m_cells.lock();
  auto vlocked = cells.lock();
  if (mlocked == vlocked)
  {
    return false;
  }
  m_cells = vlocked;
  return true;
}

const std::weak_ptr<smtk::markup::AssignedIds>& Grid::cells() const
{
  return m_cells;
}

std::weak_ptr<smtk::markup::AssignedIds>& Grid::cells()
{
  return m_cells;
}


} // namespace markup
} // namespace smtk
