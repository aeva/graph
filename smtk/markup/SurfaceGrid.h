// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_SurfaceGrid_h
#define smtk_markup_SurfaceGrid_h

#include "smtk/markup/Grid.h"

#include <memory>

#include "vtkSmartPointer.h"

class vtkPolyData;

namespace smtk
{
namespace markup
{

/// A collection of unstructured, possibly-nonconforming surface cells.
class SMTKMARKUP_EXPORT SurfaceGrid : public smtk::markup::Grid
{
public:
  smtkTypeMacro(smtk::markup::SurfaceGrid);
  smtkSuperclassMacro(smtk::markup::Grid);

  template<typename... Args>
  SurfaceGrid(Args&&... args)
    : smtk::markup::Grid(std::forward<Args>(args)...)
  {
  }

  ~SurfaceGrid() override;

  /// Provide an initializer for resources to call after construction.
  void initialize(const nlohmann::json& data, json::Helper& helper) override;

  bool setData(const vtkSmartPointer<vtkPolyData>& data);
  const vtkSmartPointer<vtkPolyData>& data() const;
  vtkSmartPointer<vtkPolyData>& data();

protected:
  vtkSmartPointer<vtkPolyData> m_data;
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_SurfaceGrid_h
