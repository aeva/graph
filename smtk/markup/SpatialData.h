// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_SpatialData_h
#define smtk_markup_SpatialData_h

#include "smtk/markup/Component.h"

namespace smtk
{
namespace markup
{

class SMTKMARKUP_EXPORT SpatialData : public smtk::markup::Component
{
public:
  smtkTypeMacro(smtk::markup::SpatialData);
  smtkSuperclassMacro(smtk::markup::Component);

  template<typename... Args>
  SpatialData(Args&&... args)
    : smtk::markup::Component(std::forward<Args>(args)...)
  {
  }

  ~SpatialData() override;

  /// Provide an initializer for resources to call after construction.
  void initialize(const nlohmann::json& data, json::Helper& helper) override;

protected:
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_SpatialData_h
