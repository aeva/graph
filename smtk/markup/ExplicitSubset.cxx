// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/ExplicitSubset.h"

namespace smtk
{
namespace markup
{

ExplicitSubset::~ExplicitSubset() = default;

void ExplicitSubset::initialize(const nlohmann::json& data, json::Helper& helper)
{
}

bool ExplicitSubset::setMembers(const std::set<smtk::markup::AssignedIds::IdType>& members)
{
  if (m_members == members)
  {
    return false;
  }
  m_members = members;
  return true;
}

const std::set<smtk::markup::AssignedIds::IdType>& ExplicitSubset::members() const
{
  return m_members;
}

std::set<smtk::markup::AssignedIds::IdType>& ExplicitSubset::members()
{
  return m_members;
}


} // namespace markup
} // namespace smtk
