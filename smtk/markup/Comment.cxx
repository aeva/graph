// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/Comment.h"

namespace smtk
{
namespace markup
{

Comment::~Comment() = default;

bool Comment::setData(const smtk::string::Token& data)
{
  if (m_data == data)
  {
    return false;
  }
  m_data = data;
  return true;
}

const smtk::string::Token& Comment::data() const
{
  return m_data;
}

smtk::string::Token& Comment::data()
{
  return m_data;
}

bool Comment::setMimetype(const smtk::string::Token& mimetype)
{
  if (m_mimetype == mimetype)
  {
    return false;
  }
  m_mimetype = mimetype;
  return true;
}

const smtk::string::Token& Comment::mimetype() const
{
  return m_mimetype;
}

smtk::string::Token& Comment::mimetype()
{
  return m_mimetype;
}


} // namespace markup
} // namespace smtk
