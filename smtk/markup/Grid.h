// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_Grid_h
#define smtk_markup_Grid_h

#include "smtk/markup/DiscreteGeometry.h"

#include "smtk/markup/AssignedIds.h"

namespace smtk
{
namespace markup
{

/// A base class for collections of unstructured cells.
class SMTKMARKUP_EXPORT Grid : public smtk::markup::DiscreteGeometry
{
public:
  smtkTypeMacro(smtk::markup::Grid);
  smtkSuperclassMacro(smtk::markup::DiscreteGeometry);

  template<typename... Args>
  Grid(Args&&... args)
    : smtk::markup::DiscreteGeometry(std::forward<Args>(args)...)
  {
  }

  ~Grid() override;

  /// Provide an initializer for resources to call after construction.
  void initialize(const nlohmann::json& data, json::Helper& helper) override;

  bool setPoints(const std::weak_ptr<smtk::markup::AssignedIds>& points);
  const std::weak_ptr<smtk::markup::AssignedIds>& points() const;
  std::weak_ptr<smtk::markup::AssignedIds>& points();

  bool setCells(const std::weak_ptr<smtk::markup::AssignedIds>& cells);
  const std::weak_ptr<smtk::markup::AssignedIds>& cells() const;
  std::weak_ptr<smtk::markup::AssignedIds>& cells();

protected:
  std::weak_ptr<smtk::markup::AssignedIds> m_points;
  std::weak_ptr<smtk::markup::AssignedIds> m_cells;
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_Grid_h
