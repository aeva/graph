// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_Collection_h
#define smtk_markup_Collection_h

#include "smtk/markup/Component.h"

#include "smtk/markup/IdAllotment.h"

namespace smtk
{
namespace markup
{

/// A set of components owned by this collection (i.e., held by shared pointer).
class SMTKMARKUP_EXPORT Collection : public smtk::markup::Component
{
public:
  smtkTypeMacro(smtk::markup::Collection);
  smtkSuperclassMacro(smtk::markup::Component);

  template<typename... Args>
  Collection(Args&&... args)
    : smtk::markup::Component(std::forward<Args>(args)...)
  {
  }

  ~Collection() override;

  bool setMembers(const std::set<std::weak_ptr<smtk::markup::Component>, std::owner_less<std::weak_ptr<smtk::markup::Component>>>& members);

  bool setKeys(const std::weak_ptr<smtk::markup::IdAllotment>& keys);
  const std::weak_ptr<smtk::markup::IdAllotment>& keys() const;
  std::weak_ptr<smtk::markup::IdAllotment>& keys();

  const std::unordered_map<smtk::markup::AssignedIds::IdType, std::weak_ptr<smtk::markup::Component>>& members() const;
  std::unordered_map<smtk::markup::AssignedIds::IdType, std::weak_ptr<smtk::markup::Component>>& members();

protected:
  std::weak_ptr<smtk::markup::IdAllotment> m_keys;
  std::unordered_map<smtk::markup::AssignedIds::IdType, std::weak_ptr<smtk::markup::Component>> m_members;
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_Collection_h
