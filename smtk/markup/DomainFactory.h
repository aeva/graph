// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).

#ifndef smtk_markup_DomainFactory_h
#define smtk_markup_DomainFactory_h

#include "smtk/markup/Component.h"

#include "smtk/common/Factory.h"
#include "smtk/common/UUID.h"

#include "nlohmann/json.hpp"

namespace smtk
{
namespace markup
{

class Resource;

using DomainFactory =
  smtk::common::Factory<
    smtk::markup::Domain,
    // Possible constructor arguments:
    // 0. Nothing:
    void,
    // 1. Just a token (name):
    smtk::string::Token,
    // 2. A JSON object
    const nlohmann::json&
  >;

} // namespace markup
} // namespace smtk

#endif // smtk_markup_DomainFactory_h
