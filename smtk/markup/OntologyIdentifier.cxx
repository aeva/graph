// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/OntologyIdentifier.h"

namespace smtk
{
namespace markup
{

OntologyIdentifier::~OntologyIdentifier() = default;

void OntologyIdentifier::initialize(const nlohmann::json& data, json::Helper& helper)
{
}

bool OntologyIdentifier::setOntologyId(const OntologyIdentifier::Token& ontologyId)
{
  if (m_ontologyId == ontologyId)
  {
    return false;
  }
  m_ontologyId = ontologyId;
  return true;
}

const OntologyIdentifier::Token& OntologyIdentifier::ontologyId() const
{
  return m_ontologyId;
}

OntologyIdentifier::Token& OntologyIdentifier::ontologyId()
{
  return m_ontologyId;
}


} // namespace markup
} // namespace smtk
