// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_Landmark_h
#define smtk_markup_Landmark_h

#include "smtk/markup/Feature.h"

namespace smtk
{
namespace markup
{

class SMTKMARKUP_EXPORT Landmark : public smtk::markup::Feature
{
public:
  smtkTypeMacro(smtk::markup::Landmark);
  smtkSuperclassMacro(smtk::markup::Feature);

  template<typename... Args>
  Landmark(Args&&... args)
    : smtk::markup::Feature(std::forward<Args>(args)...)
  {
  }

  ~Landmark() override;

  /// Provide an initializer for resources to call after construction.
  void initialize(const nlohmann::json& data, json::Helper& helper) override;

protected:
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_Landmark_h
