#include "smtk/markup/AssignedIds.h"
#include "smtk/markup/Component.h"
#include "smtk/markup/Label.h"
#include "smtk/markup/Group.h"
#include "smtk/markup/Registrar.h"
#include "smtk/markup/Resource.h"
#include "smtk/markup/operators/Read.h"
#include "smtk/markup/operators/Write.h"

#include "smtk/plugin/Registry.h"

#include "smtk/operation/Manager.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ReferenceItem.h"
#include "smtk/attribute/ResourceItem.h"

#include "smtk/resource/Manager.h"

#include "smtk/common/Managers.h"
#include "smtk/common/UUID.h"
#include "smtk/common/testing/cxx/helpers.h"

#include "nlohmann/json.hpp"

#include <cstdio>
#include <iostream>
#include <sstream>

using namespace smtk::markup;
using namespace json;

namespace
{

smtk::common::Managers::Ptr testRegistrar()
{
  auto managers = smtk::common::Managers::create();
  auto resourceManager = smtk::resource::Manager::create();
  auto operationManager = smtk::operation::Manager::create();
  managers->insert_or_assign(resourceManager);
  managers->insert_or_assign(operationManager);

  auto markupRegistry =
    smtk::plugin::addToManagers<smtk::markup::Registrar>(
      resourceManager, operationManager);

  // Register the resource manager to the operation manager (newly created
  // resources will be automatically registered to the resource manager).
  operationManager->registerResourceManager(resourceManager);

  return managers;
}

std::string testCreateAndWrite()
{
  auto resource = smtk::markup::Resource::create();
  std::ostringstream filename;
  filename << tmpnam(nullptr) << ".smtk";
  resource->setLocation(filename.str());

  auto label = resource->createNode<Label>();
  label->setName("foo");
  auto group = std::make_shared<Group>(resource, smtk::common::UUID::random());
  resource->add(group);
  group->setName("barf");
  /*
  auto group = resource->createNode<Group>();
  group->setName("barf");
  */
  auto components = resource->filter("*");
  std::size_t nn = components.size();
  std::cout << "Created " << resource << " with " << nn << " components.\n";
  for (const auto& component : components)
  {
    std::cout << "  " << component->typeName() << ": " << component->name() << "\n";
  }
  test(nn == 2, "Expected to create 2 components.");

  components = resource->filter("smtk::markup::Label");
  nn = components.size();
  std::cout << "Found " << nn << " labels\n";

  auto anotherLabel = resource->createNode<Label>(
    R"({"name": "This is a really long descriptive label with no purpose."})"_json);
  auto anotherGroup = resource->createNode<Group>(
    R"({"name": "baz"})"_json);

  label->get<LabelSubject>().insert(*group);
  resource->connect<LabelSubject>(*anotherLabel, *anotherGroup);
  // group->get<Labels>().insert(*label);

  // Verify that both arcs (Labels and LabelSubject) were created.
  test(label->get<LabelSubject>().to().id() == group->id(), "Label has no subject.");
  test(group->get<Labels>().count() == 1, "Group has no label or multiple labels.");
  test(group->get<Labels>().to().find(*label) != group->get<Labels>().to().end(), "Group does not have label.");

  test(anotherLabel->get<LabelSubject>().to().id() == anotherGroup->id(), "AnotherLabel has no subject.");
  test(anotherGroup->get<Labels>().count() == 1, "AnotherGroup has no label or multiple labels.");
  test(
    anotherGroup->get<Labels>().to().find(*anotherLabel) !=
    anotherGroup->get<Labels>().to().end(),
    "AnotherGroup does not have label.");

  std::cout << "Writing " << filename.str() << "\n";

  auto write = smtk::markup::Write::create();
  write->parameters()->associations()->appendValue(resource);
  write->operate();
  return filename.str(); // or resource->location();
}

std::string testReadAndWrite(const std::string& filename1)
{
  bool ok = true;
  auto read = smtk::markup::Read::create();
  ok = read->parameters()->findFile("filename")->setValue(filename1);
  test(ok, "Could not set filename.");
  std::cout << "Reading " << read->parameters()->findFile("filename")->value() << "\n";
  auto result = read->operate();
  auto resource = result->findResource("resource")->valueAs<smtk::markup::Resource>();
  std::cout << "Read    " << result->findInt("outcome")->value() << " " << resource << "\n";

  // Rename the file we just read to make room for the second generation copy.
  std::ostringstream filename2;
  filename2 << tmpnam(nullptr) << ".smtk";
  std::cout << "Rename  " << filename2.str() << "\n";
  std::rename(filename1.c_str(), filename2.str().c_str());

  // Write our resource back to filename1.
  auto write = smtk::markup::Write::create();
  write->parameters()->associations()->appendValue(resource);
  result = write->operate();
  std::cout << "Wrote   " << result->findInt("outcome")->value() << " " << resource << "\n";
  // std::cout << "Wrote   " << resource->location() << "? " << result->findInt("outcome")->value() << "\n";

  return filename2.str();
}

bool testFileContentsMatch(const std::string& filename1, const std::string& filename2)
{
  return true;
}

} // anonymous namespace

int TestResource(int argc, char** argv)
{
  auto managers = testRegistrar();
  std::string filename1 = testCreateAndWrite();
  std::string filename2 = testReadAndWrite(filename1);
  bool ok = testFileContentsMatch(filename1, filename2);
  return ok ? 0 : 1;
}
