#include "smtk/markup/AssignedIds.h"
#include "smtk/markup/Component.h"
#include "smtk/markup/Label.h"
#include "smtk/markup/Group.h"
#include "smtk/markup/Resource.h"

#include "smtk/common/UUID.h"
#include "smtk/common/testing/cxx/helpers.h"

#include <iostream>

using namespace smtk::markup;

namespace
{

void printAssignments(
  const std::set<std::shared_ptr<AssignedIds>>& assignments,
  const std::string& summary)
{
  std::cout << summary << "\n";
  for (const auto& assignment : assignments)
  {
    std::cout
      << "  " << assignment.get()
      << " " << static_cast<int>(assignment->nature())
      << " [" << assignment->range()[0] << ", " << assignment->range()[1] << "["
      << "\n";
  }
}

} // anonymous namespace

int TestIds(int argc, char** argv)
{
  using namespace smtk::string;
  // Create an IdSpace:
  auto pointIds = std::make_shared<IdSpace>("points"_token);

  // Allocate some IDs (of varying Nature) in the space:
  IdSpace::IdType nn;
  auto primary1 = pointIds->requestRange(Nature::Primary, 10);
  auto primary2 = pointIds->requestRange(Nature::Primary, 10, 11);
  auto primary3 = pointIds->requestRange(Nature::Primary, 10);
  auto reference1 = pointIds->requestRange(Nature::Referential, 10, 5);
  auto reference2 = pointIds->requestRange(Nature::Referential, 20, 5);

  nn = pointIds->numberOfIdsInRangeOfNature(1, 31, Nature::Primary);
  std::cout << nn << " primary IDs in [1,31[\n";
  test(nn == 30, "Expected 30 primary IDs.");

  nn = pointIds->numberOfIdsInRangeOfNature(1, 31, Nature::Referential);
  std::cout << nn << " referential IDs in [1,31[\n";
  test(nn == 20, "Expected 20 referential IDs.");

  auto reference3 = pointIds->requestRange(Nature::Referential, 20, 20); // should fail
  auto nonexclsv1 = pointIds->requestRange(Nature::NonExclusive, 20, 20); // should fail

  test(!reference3, "Expected reference beyond end of assignments to fail.");
  test(!nonexclsv1, "Expected non-exclusive allocation overlapping primary assignments to fail.");

  auto nonexclsv2 = pointIds->requestRange(Nature::NonExclusive, 10, 40); // should succeed
  auto nonexclsv3 = pointIds->requestRange(Nature::NonExclusive, 5, 45); // should succeed
                                                                         //
  nn = pointIds->numberOfIdsInRangeOfNature(1, 51, Nature::NonExclusive);
  std::cout << nn << " non-exclusive IDs in [1,51[\n";
  test(nn == 10, "Expected 10 non-exclusive IDs.");

  nn = pointIds->numberOfIdsInRangeOfNature(1, 31, Nature::Unassigned);
  std::cout << nn << " unassigned IDs in [1,31[\n";
  test(nn == 0, "Expected 0 unassigned IDs.");

  std::cout
    << "IdSpace <" << pointIds->name().data() << ">"
    << " range [" << pointIds->range()[0] << ", " << pointIds->range()[1]
    << "[\n";

  test(!reference3, "Expected referential request not fully covered by primary IDs to fail.");
  test(!nonexclsv1, "Expected non-exclusive request not fully covered by primary Ids to fail.");

  auto assignments = pointIds->assignedIds(10,12);
  printAssignments(assignments, "[10, 12[");
  std::array<std::size_t, 4> counts{0, 0, 0, 0};
  for (const auto& assignment : assignments)
  {
    ++counts[static_cast<int>(assignment->nature())];
  }
  test(counts[static_cast<int>(Nature::Primary)] == 2, "Expected [10,12[ to overlap two primary assignments.");
  test(counts[static_cast<int>(Nature::Referential)] == 2, "Expected [10,12[ to overlap two referential assignments.");
  test(counts[static_cast<int>(Nature::NonExclusive)] == 0, "Expected [10,12[ to overlap zero non-exclusive assignments.");

  assignments = pointIds->assignedIds(1,2);
  printAssignments(assignments, "[1, 2[");
  test(assignments.size() == 1, "Expected [1,2[ to overlap a single set of assigned IDs.");

  test(primary1->range()[0] == 1, "Expected first range to start at 1.");
  test(primary1->range()[1] == 11, "Expected first range to end at 11.");

  test(primary2->range()[0] == 11, "Expected second range to start at 11.");
  test(primary2->range()[1] == 21, "Expected second range to end at 21.");

  test(primary3->range()[0] == 21, "Expected second range to start at 21.");
  test(primary3->range()[1] == 31, "Expected second range to end at 31.");

  // Now verify that deleting assigned IDs removes them from the id-space.
  reference2 = nullptr; // This should cause the assigned IDs to be destroyed.
  nn = pointIds->numberOfIdsInRangeOfNature(1, 31, Nature::Referential);
  std::cout << nn << " referential IDs in [1,31[ after removing reference2.\n";
  test(nn == 10, "Expected 10 referential IDs.");

  // TODO: Warn or fail if referential entries would remain in a range when
  //       removing primary or non-exclusive entries.
  return 0;
}
