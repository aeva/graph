// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/ImplicitShape.h"

#include "vtkImplicitFunction.h"
#include "smtk/markup/Domain.h"

namespace smtk
{
namespace markup
{

ImplicitShape::~ImplicitShape() = default;

void ImplicitShape::initialize(const nlohmann::json& data, json::Helper& helper)
{
}

bool ImplicitShape::setDomain(const std::weak_ptr<smtk::markup::Domain>& domain)
{
  auto mlocked = m_domain.lock();
  auto vlocked = domain.lock();
  if (mlocked == vlocked)
  {
    return false;
  }
  m_domain = vlocked;
  return true;
}

const std::weak_ptr<smtk::markup::Domain>& ImplicitShape::domain() const
{
  return m_domain;
}

std::weak_ptr<smtk::markup::Domain>& ImplicitShape::domain()
{
  return m_domain;
}

bool ImplicitShape::setFunction(const vtkSmartPointer<vtkImplicitFunction>& function)
{
  if (m_function == function)
  {
    return false;
  }
  m_function = function;
  return true;
}

const vtkSmartPointer<vtkImplicitFunction>& ImplicitShape::function() const
{
  return m_function;
}

vtkSmartPointer<vtkImplicitFunction>& ImplicitShape::function()
{
  return m_function;
}


} // namespace markup
} // namespace smtk
