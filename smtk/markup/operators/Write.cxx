//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/markup/operators/Write.h"

#include "smtk/markup/Resource.h"
#include "smtk/markup/Write_xml.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"

#include "smtk/markup/json/jsonResource.h"

#include "smtk/common/Paths.h"

using namespace smtk::model;

namespace smtk
{
namespace markup
{

bool Write::ableToOperate()
{
  if (!this->Superclass::ableToOperate())
  {
    return false;
  }

  auto associations = this->parameters()->associations();
  if (associations->numberOfValues() != 1 || !associations->isSet())
  {
    smtkWarningMacro(this->log(), "A resource must be provided.");
    return false;
  }

  auto resource = associations->valueAs<smtk::markup::Resource>();
  if (!resource || resource->location().empty())
  {
    smtkWarningMacro(this->log(), "Resource must have a valid location and be a markup resource.");
    return false;
  }

  // TODO: We could also check that the location is writable.

  return true;
}

Write::Result Write::operateInternal()
{
  auto resourceItem = this->parameters()->associations();

  smtk::markup::Resource::Ptr rsrc =
    std::dynamic_pointer_cast<smtk::markup::Resource>(resourceItem->value());

  // Serialize resource into a set of JSON records:
  nlohmann::json j = rsrc;

  if (j.is_null())
  {
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);
  }

  std::string fileDirectory = smtk::common::Paths::directory(rsrc->location()) + "/";

  // Write JSON records to the specified URL:
  std::ofstream jsonFile(rsrc->location(), std::ios::out | std::ios::trunc);
  bool ok = jsonFile.good() && !jsonFile.fail();
  if (ok)
  {
    jsonFile << j;
    jsonFile.close();
  }

  return ok ? this->createResult(smtk::operation::Operation::Outcome::SUCCEEDED)
            : this->createResult(smtk::operation::Operation::Outcome::FAILED);
}

const char* Write::xmlDescription() const
{
  return Write_xml;
}

void Write::markModifiedResources(Write::Result& /*unused*/)
{
  auto resourceItem = this->parameters()->associations();
  for (auto rit = resourceItem->begin(); rit != resourceItem->end(); ++rit)
  {
    auto resource = std::dynamic_pointer_cast<smtk::resource::Resource>(*rit);

    // Set the resource as unmodified from its persistent (i.e. on-disk) state
    resource->setClean(true);
  }
}

bool write(
  const smtk::resource::ResourcePtr& resource,
  const std::shared_ptr<smtk::common::Managers>& managers)
{
  Write::Ptr write = Write::create();
  write->setManagers(managers);
  write->parameters()->associate(resource);
  Write::Result result = write->operate();
  return (result->findInt("outcome")->value() == static_cast<int>(Write::Outcome::SUCCEEDED));
}

} // namespace markup
} // namespace smtk
