// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/CompositeSubset.h"

#include "smtk/markup/Subset.h"

namespace smtk
{
namespace markup
{

CompositeSubset::~CompositeSubset() = default;

bool CompositeSubset::setMembers(const std::set<std::weak_ptr<smtk::markup::Subset>, std::owner_less<std::weak_ptr<smtk::markup::Subset>>>& members)
{
  m_members = members;
  return true;
}

const std::set<std::weak_ptr<smtk::markup::Subset>, std::owner_less<std::weak_ptr<smtk::markup::Subset>>>& CompositeSubset::members() const
{
  return m_members;
}

std::set<std::weak_ptr<smtk::markup::Subset>, std::owner_less<std::weak_ptr<smtk::markup::Subset>>>& CompositeSubset::members()
{
  return m_members;
}


} // namespace markup
} // namespace smtk
