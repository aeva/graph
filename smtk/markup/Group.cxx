// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/Group.h"

namespace smtk
{
namespace markup
{

Group::~Group() = default;

void Group::initialize(const nlohmann::json& data, json::Helper& helper)
{
}

bool Group::setKeys(const std::weak_ptr<smtk::markup::AssignedIds>& keys)
{
  auto mlocked = m_keys.lock();
  auto vlocked = keys.lock();
  if (mlocked == vlocked)
  {
    return false;
  }
  m_keys = vlocked;
  return true;
}

const std::weak_ptr<smtk::markup::AssignedIds>& Group::keys() const
{
  return m_keys;
}

std::weak_ptr<smtk::markup::AssignedIds>& Group::keys()
{
  return m_keys;
}

bool Group::setOwnsMembers(const bool& ownsMembers)
{
  if (m_ownsMembers == ownsMembers)
  {
    return false;
  }
  m_ownsMembers = ownsMembers;
  return true;
}

const bool& Group::ownsMembers() const
{
  return m_ownsMembers;
}

bool& Group::ownsMembers()
{
  return m_ownsMembers;
}


} // namespace markup
} // namespace smtk
