// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/Plane.h"

namespace smtk
{
namespace markup
{

Plane::~Plane() = default;

void Plane::initialize(const nlohmann::json& data, json::Helper& helper)
{
}

bool Plane::setBasePoint(const std::array<double, 3>& basePoint)
{
  if (m_basePoint == basePoint)
  {
    return false;
  }
  m_basePoint = basePoint;
  return true;
}

const std::array<double, 3>& Plane::basePoint() const
{
  return m_basePoint;
}

std::array<double, 3>& Plane::basePoint()
{
  return m_basePoint;
}

bool Plane::setNormal(const std::array<double, 3>& normal)
{
  if (m_normal == normal)
  {
    return false;
  }
  m_normal = normal;
  return true;
}

const std::array<double, 3>& Plane::normal() const
{
  return m_normal;
}

std::array<double, 3>& Plane::normal()
{
  return m_normal;
}


} // namespace markup
} // namespace smtk
