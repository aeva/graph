// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_Plane_h
#define smtk_markup_Plane_h

#include "smtk/markup/AnalyticShape.h"

namespace smtk
{
namespace markup
{

/// A flat 3-dimensional surface identified by a point and normal.
class SMTKMARKUP_EXPORT Plane : public smtk::markup::AnalyticShape
{
public:
  smtkTypeMacro(smtk::markup::Plane);
  smtkSuperclassMacro(smtk::markup::AnalyticShape);

  template<typename... Args>
  Plane(Args&&... args)
    : smtk::markup::AnalyticShape(std::forward<Args>(args)...)
  {
  }

  ~Plane() override;

  /// Provide an initializer for resources to call after construction.
  void initialize(const nlohmann::json& data, json::Helper& helper) override;

  bool setBasePoint(const std::array<double, 3>& basePoint);
  const std::array<double, 3>& basePoint() const;
  std::array<double, 3>& basePoint();

  bool setNormal(const std::array<double, 3>& normal);
  const std::array<double, 3>& normal() const;
  std::array<double, 3>& normal();

protected:
  std::array<double, 3> m_basePoint;
  std::array<double, 3> m_normal;
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_Plane_h
