// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_Image_h
#define smtk_markup_Image_h

#include "smtk/markup/DiscreteGeometry.h"

#include "smtk/markup/AssignedIds.h"
#include "vtkSmartPointer.h"

class vtkImageData;

namespace smtk
{
namespace markup
{

/// An n-dimensional grid of cells.
class SMTKMARKUP_EXPORT Image : public smtk::markup::DiscreteGeometry
{
public:
  smtkTypeMacro(smtk::markup::Image);
  smtkSuperclassMacro(smtk::markup::DiscreteGeometry);

  template<typename... Args>
  Image(Args&&... args)
    : smtk::markup::DiscreteGeometry(std::forward<Args>(args)...)
  {
  }

  ~Image() override;

  /// Provide an initializer for resources to call after construction.
  void initialize(const nlohmann::json& data, json::Helper& helper) override;

  bool setAxes(const std::vector<std::weak_ptr<smtk::markup::AssignedIds>>& axes);
  const std::vector<std::weak_ptr<smtk::markup::AssignedIds>>& axes() const;
  std::vector<std::weak_ptr<smtk::markup::AssignedIds>>& axes();

  bool setPoints(const std::weak_ptr<smtk::markup::AssignedIds>& points);
  const std::weak_ptr<smtk::markup::AssignedIds>& points() const;
  std::weak_ptr<smtk::markup::AssignedIds>& points();

  bool setCells(const std::weak_ptr<smtk::markup::AssignedIds>& cells);
  const std::weak_ptr<smtk::markup::AssignedIds>& cells() const;
  std::weak_ptr<smtk::markup::AssignedIds>& cells();

  bool setData(const vtkSmartPointer<vtkImageData>& data);
  const vtkSmartPointer<vtkImageData>& data() const;
  vtkSmartPointer<vtkImageData>& data();

protected:
  std::vector<std::weak_ptr<smtk::markup::AssignedIds>> m_axes;
  std::weak_ptr<smtk::markup::AssignedIds> m_points;
  std::weak_ptr<smtk::markup::AssignedIds> m_cells;
  vtkSmartPointer<vtkImageData> m_data;
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_Image_h
