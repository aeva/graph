// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_RangedSubset_h
#define smtk_markup_RangedSubset_h

#include "smtk/markup/Subset.h"

namespace smtk
{
namespace markup
{

/// A subset whose members are stored as ranges rather than single values.
class SMTKMARKUP_EXPORT RangedSubset : public smtk::markup::Subset
{
public:
  smtkTypeMacro(smtk::markup::RangedSubset);
  smtkSuperclassMacro(smtk::markup::Subset);

  template<typename... Args>
  RangedSubset(Args&&... args)
    : smtk::markup::Subset(std::forward<Args>(args)...)
  {
  }

  ~RangedSubset() override;

  /// Provide an initializer for resources to call after construction.
  void initialize(const nlohmann::json& data, json::Helper& helper) override;

  bool setMembers(const std::set<std::array<smtk::markup::AssignedIds::IdType, 2>>& members);
  const std::set<std::array<smtk::markup::AssignedIds::IdType, 2>>& members() const;
  std::set<std::array<smtk::markup::AssignedIds::IdType, 2>>& members();

protected:
  std::set<std::array<smtk::markup::AssignedIds::IdType, 2>> m_members;
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_RangedSubset_h
