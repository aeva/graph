// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/RangedSubset.h"

namespace smtk
{
namespace markup
{

RangedSubset::~RangedSubset() = default;

void RangedSubset::initialize(const nlohmann::json& data, json::Helper& helper)
{
}

bool RangedSubset::setMembers(const std::set<std::array<smtk::markup::AssignedIds::IdType, 2>>& members)
{
  if (m_members == members)
  {
    return false;
  }
  m_members = members;
  return true;
}

const std::set<std::array<smtk::markup::AssignedIds::IdType, 2>>& RangedSubset::members() const
{
  return m_members;
}

std::set<std::array<smtk::markup::AssignedIds::IdType, 2>>& RangedSubset::members()
{
  return m_members;
}


} // namespace markup
} // namespace smtk
