#include "smtk/markup/json/jsonDomainMap.h"
#include "smtk/markup/json/jsonDomain.h"

namespace smtk
{
namespace markup
{

void to_json(nlohmann::json& j, const smtk::markup::DomainMap& domainMap)
{
  j = nlohmann::json::array();
  for (const auto& key : domainMap.keys())
  {
    nlohmann::json jdomain = domainMap[key];
    j.push_back(jdomain);
  }
}

void from_json(const nlohmann::json& j, smtk::markup::DomainMap& domainMap)
{
  // TODO
#if 0
  auto& helper = json::Helper::instance();
  auto typeName = j.at("type").get<std::string>();
  auto name = j.at("name").get<std::string>();
  auto uid = j.at("id").get<smtk::common::UUID>();
  component = Resource::nodeFactory().makeFromName(typeName, &j, helper.resource(), uid);
#endif
}

} // namespace markup
} // namespace smtk
