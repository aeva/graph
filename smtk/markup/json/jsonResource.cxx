#include "smtk/markup/json/jsonResource.h"

#include "smtk/markup/Component.h"
#include "smtk/markup/Resource.h"
#include "smtk/markup/arcs/Groups.h"
#include "smtk/markup/arcs/Labels.h"
#include "smtk/markup/detail/NodeContainer.h"
#include "smtk/markup/json/Helper.h"
#include "smtk/markup/json/jsonComponent.h"
#include "smtk/markup/json/jsonDomainMap.h"

#include "smtk/common/json/jsonTypeMap.h"
#include "smtk/resource/json/jsonResource.h"
#include "smtk/graph/ArcMap.h"
#include "smtk/string/Manager.h"
#include "smtk/string/Token.h"
#include "smtk/string/json/jsonManager.h"
#include "smtk/string/json/jsonToken.h"

using ArcMap = smtk::graph::ArcMap;

namespace smtk
{
namespace markup
{

template<typename Src, typename Dst, typename ArcType>
void to_json(nlohmann::json& j, const smtk::graph::Arc<Src, Dst, ArcType>& arc)
{
  j["from"] = arc.from().id();
  j["to"] = arc.to().id();
}

template<typename Src, typename Dst, typename ArcType>
void to_json(nlohmann::json& j, const smtk::graph::Arcs<Src, Dst, ArcType>& arcs)
{
  j["from"] = arcs.from().id();
  auto& dest = j["to"];
  for (const auto& node : arcs.to())
  {
    dest.push_back(node.get().id());
  }
}

template<typename Src, typename Dst, typename ArcType>
void from_json(const nlohmann::json& j, smtk::graph::Arc<Src, Dst, ArcType>& arc)
{
  auto resource = json::Helper::instance().resource();
  auto from = std::dynamic_pointer_cast<Src>(resource->find(j.at("from").get<smtk::common::UUID>()));
  auto to = std::dynamic_pointer_cast<Dst>(resource->find(j.at("to").get<smtk::common::UUID>()));
  arc = ArcType(from, to);
}

template<typename Src, typename Dst, typename ArcType>
void from_json(const nlohmann::json& j, smtk::graph::Arcs<Src, Dst, ArcType>& arcs)
{
  auto resource = json::Helper::instance().resource();
  auto from = std::dynamic_pointer_cast<Src>(resource->find(j.at("from").get<smtk::common::UUID>()));
  arcs = ArcType(from);
  for (const auto& jto : j.at("to"))
  {
    auto to = std::dynamic_pointer_cast<Dst>(resource->find(jto.get<smtk::common::UUID>()));
    arcs.insert(to);
  }
}

template<typename Src, typename Dst, typename ArcType>
void from_json(const nlohmann::json& j, smtk::graph::OrderedArcs<Src, Dst, ArcType>& arcs)
{
  auto resource = json::Helper::instance().resource();
  auto from = std::dynamic_pointer_cast<Src>(resource->find(j.at("from").get<smtk::common::UUID>()));
  arcs = ArcType(from);
  const auto& jAllTo = j.at("to");
  arcs.to().reserve(jAllTo.size());
  for (const auto& jto : jAllTo)
  {
    auto to = std::dynamic_pointer_cast<Dst>(resource->find(jto.get<smtk::common::UUID>()));
    arcs.insert(to);
  }
}

template<std::size_t I, typename Tuple, typename ResourceType>
inline typename std::enable_if<I != std::tuple_size<Tuple>::value, void>::type deserializeArcsOfType(
  const nlohmann::json& jj,
  const std::shared_ptr<ResourceType>& resource,
  ArcMap& arcMap)
{
  using ArcType = typename std::tuple_element<I, Tuple>::type;
  auto arcType = smtk::common::typeName<ArcType>();
  auto it = jj.find(arcType);
  if (it != jj.end())
  {
    /*
    if (!resource->arcs().template contains<ArcType>(arcType))
    {
      // FIXME: URHERE Cannot construct ArcType without a from node? But I want to insert a TypeMapEntry<UUID, ArcType>?
      resource->arcs().template insert<ArcType>(arcType, ArcType{});
    }
    auto& arcEntry(resource->arcs().template get<ArcType>());
    */
    std::vector<std::reference_wrapper<typename ArcType::ToType>> toNodes;
    for (const auto jarc : *it)
    {
      toNodes.clear();
      smtk::common::UUID fromId = jarc.at("from").template get<smtk::common::UUID>();
      auto fromNode = std::dynamic_pointer_cast<typename ArcType::FromType>(resource->find(fromId));
      if (fromNode)
      {
        toNodes.reserve(jarc.size());
        for (const auto jto : jarc.at("to"))
        {
          smtk::common::UUID toId = jto;
          auto toNode = std::dynamic_pointer_cast<typename ArcType::ToType>(resource->find(toId));
          if (toNode)
          {
            // std::weak_ptr<typename ArcType::ToType> weakRef(toNode);
            toNodes.push_back(std::ref(*toNode));
          }
        }
        fromNode->template set<ArcType>(toNodes.begin(), toNodes.end());
        // std::cout << "Extract arc of type " << arcType << " from " << fromId << "\n";
        // ArcType value(fromId);
        // from_json(jarc, value);
        // arcEntry.emplace(value.from(), value);
      }
      else
      {
        smtkErrorMacro(smtk::io::Logger::instance(),
          "Could not find node " << fromId << ". Skipping.");
      }
    }
  }

  // Now process the next entry in the tuple:
  deserializeArcsOfType<I + 1, Tuple>(jj, resource, arcMap);
}

template<std::size_t I, typename Tuple, typename ResourceType>
inline typename std::enable_if<I == std::tuple_size<Tuple>::value, void>::type deserializeArcsOfType(
  const nlohmann::json& jj,
  const std::shared_ptr<ResourceType>& resource,
  ArcMap& arcMap)
{
  (void)jj;
  (void)resource;
  (void)arcMap;
}

template<typename ResourceType>
void deserializeArcs(
  const nlohmann::json& jj,
  const std::shared_ptr<ResourceType>& resource,
  ArcMap& arcMap)
{
  deserializeArcsOfType<0, typename ResourceType::TypeTraits::ArcTypes>(jj, resource, arcMap);
}

void from_json(const nlohmann::json& jj, ArcMap& arcMap)
{
  // Erase all pre-existing arcs
  arcMap.data().clear();
  // Keep track of visited arc types. At the end, we'll signal an error if there
  // were arc types present in the JSON that this resource's traits do not handle.
  std::set<std::string> visitedArcTypes;
  auto resource = json::Helper::instance().resource();
  deserializeArcs(jj, resource, arcMap);
}

template<std::size_t I, typename Tuple, typename ResourceType>
inline typename std::enable_if<I != std::tuple_size<Tuple>::value, void>::type serializeArcsOfType(
  const std::shared_ptr<ResourceType>& resource, nlohmann::json& arcs)
{
  using ArcType = typename std::tuple_element<I, Tuple>::type;
  auto arcType = smtk::common::typeName<ArcType>();
  try
  {
    const auto& arcEntry(resource->arcs().template get<ArcType>());
    auto jarr = nlohmann::json::array();
    for (const auto& entry : arcEntry.data())
    {
      nlohmann::json j = entry.second;
      jarr.push_back(j);
    }
    arcs[arcType] = jarr;
  }
  catch (std::domain_error&)
  {
    // No entry, so do nothing.
  }

  // Now process the next entry in the tuple:
  serializeArcsOfType<I + 1, Tuple>(resource, arcs);
}

template<std::size_t I, typename Tuple, typename ResourceType>
inline typename std::enable_if<I == std::tuple_size<Tuple>::value, void>::type serializeArcsOfType(
  const std::shared_ptr<ResourceType>& resource, nlohmann::json& arcs)
{
}

template<typename ResourceType>
nlohmann::json serializeArcs(const std::shared_ptr<ResourceType>& resource)
{
  auto arcs = nlohmann::json::object();
  serializeArcsOfType<0, typename ResourceType::TypeTraits::ArcTypes>(resource, arcs);
  return arcs;
}

void to_json(nlohmann::json& j, const smtk::markup::Resource::Ptr& resource)
{
  // Add version number and other information inherited from parent.
  smtk::resource::to_json(j, std::static_pointer_cast<smtk::resource::Resource>(resource));

  // Record domains.
  j["domains"] = resource->domains();

  // Record nodes.
  auto jnodes = nlohmann::json::array();
  std::function<void(const std::shared_ptr<smtk::resource::Component>&)> visitor =
    [&jnodes](const std::shared_ptr<smtk::resource::Component>& rnode)
    {
      auto node = dynamic_pointer_cast<smtk::markup::Component>(rnode);
      nlohmann::json jnode = node;
      jnodes.push_back(jnode);
    };
  resource->visit(visitor);
  j["nodes"] = jnodes;

  // Record arcs.
  // We can't just do this: j["arcs"] = resource->arcs(); because the
  // arc classes don't share a common base type. (If they did, we could
  // use the same trick smtk::common::Links does.)
  j["arcs"] = serializeArcs(resource);

  // Record string-token hashes.
  // Some nodes may use string tokens, so we must serialize that map if it exists.
  auto& tokenManager = smtk::string::Token::manager();
  if (!tokenManager.empty())
  {
    j["tokens"] = tokenManager.shared_from_this();
  }

  // TODO: Opportunity to record other application data.
  const auto& managers = json::Helper::instance().managers();
  if (managers)
  {
    std::cout << "Have managers\n";
  }
}

void from_json(const nlohmann::json& j, smtk::markup::Resource::Ptr& resource)
{
  if (!resource)
  {
    resource = smtk::markup::Resource::create();
  }
  auto tmp = std::static_pointer_cast<smtk::resource::Resource>(resource);
  smtk::resource::from_json(j, tmp);
  resource->m_domains = j.at("domains");
  // I. Insert nodes. Initialize them later (so they can freely
  // assume other nodes are present when configuring themselves).
  for (const auto& jnode : j.at("nodes"))
  {
    auto nodeType = jnode.at("type").get<std::string>();
    auto nodeId = jnode.at("id").get<smtk::common::UUID>();
    // std::cout << "  Create " << nodeType << " " << (smtk::markup::Resource::nodeFactory().contains(nodeType) ? "Y" : "N") << "\n";
    auto node = smtk::markup::Resource::nodeFactory().makeFromName(nodeType, resource, nodeId);
    std::cout << "Create " << nodeType << " id " << nodeId << " " << node << "\n";
    if (node)
    {
      resource->add(node);
    }
  }

  // II. Insert arcs. Arcs should be added before initializing nodes.
  smtk::markup::from_json(j.at("arcs"), resource->arcs());

  // III. Initialize nodes.
  //      Initializers can assume that other nodes in the resource exist
  //      and that arcs are in a valid state (making it possible to
  //      find pointers to relevant nodes by following arcs).
  auto& helper = json::Helper::instance();
  for (const auto& jnode : j.at("nodes"))
  {
    auto uid = jnode.at("id").get<smtk::common::UUID>();
    auto node = std::dynamic_pointer_cast<smtk::markup::Component>(resource->find(uid));
    if (node)
    {
      node->initialize(jnode, helper);
    }
  }
}

} // namespace markup
} // namespace smtk
