// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).

#include "smtk/markup/json/Helper.h"

#include "smtk/io/Logger.h"

#include <thread>
#include <vector>

namespace
{
thread_local std::vector<std::unique_ptr<smtk::markup::json::Helper>> g_instanceStack;
} // anonymous namespace

namespace smtk
{
namespace markup
{
namespace json
{

Helper::Helper() = default;
Helper::~Helper() = default;

Helper& Helper::instance()
{
  if (g_instanceStack.empty())
  {
    g_instanceStack.emplace_back(new Helper);
  }
  return *(g_instanceStack.back());
}

Helper& Helper::pushInstance(const std::shared_ptr<smtk::markup::Resource>& parent)
{
  std::shared_ptr<smtk::common::Managers> managers;
  if (!g_instanceStack.empty())
  {
    managers = g_instanceStack.back()->managers();
  }
  g_instanceStack.emplace_back(new Helper);
  g_instanceStack.back()->setManagers(managers);
  g_instanceStack.back()->m_resource = parent;
  g_instanceStack.back()->m_topLevel = false;
  return *(g_instanceStack.back());
}

void Helper::popInstance()
{
  if (!g_instanceStack.empty())
  {
    g_instanceStack.pop_back();
  }
}

std::size_t Helper::nestingDepth()
{
  return g_instanceStack.size();
}

void Helper::setManagers(const smtk::common::Managers::Ptr& managers)
{
  m_managers = managers;
}

smtk::common::Managers::Ptr Helper::managers()
{
  return m_managers;
}

std::shared_ptr<smtk::markup::Resource> Helper::resource() const
{
  return m_resource.lock();
}

void Helper::clear()
{
}

} // namespace json
} // namespace markup
} // namespace smtk
