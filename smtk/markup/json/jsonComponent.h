//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_markup_json_jsonComponent_h
#define smtk_markup_json_jsonComponent_h

#include "smtk/markup/Exports.h"

#include "smtk/markup/Component.h"

#include "nlohmann/json.hpp"

namespace smtk
{
namespace markup
{

SMTKMARKUP_EXPORT void to_json(nlohmann::json& j, const smtk::markup::Component::Ptr& component);

SMTKMARKUP_EXPORT void from_json(const nlohmann::json& j, smtk::markup::Component::Ptr& component);

} // namespace markup
} // namespace smtk

#endif // smtk_markup_json_jsonComponent_h
