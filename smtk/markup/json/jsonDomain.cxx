#include "smtk/markup/json/jsonDomain.h"

#include "smtk/string/json/jsonToken.h"

namespace smtk
{
namespace markup
{

void to_json(nlohmann::json& j, const std::shared_ptr<smtk::markup::Domain>& domain)
{
  if (!domain)
  {
    return;
  }
  j["name"] = domain->name();
  // TODO: dispatch based on subclass. See smtk/task/json/jsonTask for one approach.
}

void from_json(const nlohmann::json& j, std::shared_ptr<smtk::markup::Domain>& domain)
{
  // TODO
#if 0
  auto& helper = json::Helper::instance();
  auto typeName = j.at("type").get<std::string>();
  auto name = j.at("name").get<std::string>();
  auto uid = j.at("id").get<smtk::common::UUID>();
  component = Resource::nodeFactory().makeFromName(typeName, &j, helper.resource(), uid);
#endif
}

} // namespace markup
} // namespace smtk
