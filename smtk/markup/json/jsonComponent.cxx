#include "smtk/markup/json/jsonComponent.h"

#include "smtk/markup/json/Helper.h"

namespace smtk
{
namespace markup
{

void to_json(nlohmann::json& j, const smtk::markup::Component::Ptr& component)
{
  if (!component)
  {
    return;
  }
  j["type"] = component->typeName();
  j["name"] = component->name();
  j["id"] = component->id();
}

void from_json(const nlohmann::json& j, smtk::markup::Component::Ptr& component)
{
  auto& helper = json::Helper::instance();
  auto resource = helper.resource();
  auto typeName = j.at("type").get<std::string>();
  auto name = j.at("name").get<std::string>();
  auto uid = j.at("id").get<smtk::common::UUID>();
  component = Resource::nodeFactory().makeFromName(typeName, resource, uid);
  component->initialize(j, helper);
  resource->add(component);
}

} // namespace markup
} // namespace smtk
