//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_markup_json_jsonDomain_h
#define smtk_markup_json_jsonDomain_h

#include "smtk/markup/Exports.h"

#include "smtk/markup/Domain.h"

#include "nlohmann/json.hpp"

#include <memory>

namespace smtk
{
namespace markup
{

SMTKMARKUP_EXPORT void to_json(nlohmann::json& j, const std::shared_ptr<smtk::markup::Domain>& domain);

SMTKMARKUP_EXPORT void from_json(const nlohmann::json& j, std::shared_ptr<smtk::markup::Domain>& domain);

} // namespace markup
} // namespace smtk

#endif // smtk_markup_json_jsonDomain_h
