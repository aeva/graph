// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).

#ifndef smtk_markup_json_Helper_h
#define smtk_markup_json_Helper_h

#include "smtk/markup/Resource.h"

#include "smtk/common/Managers.h"
#include "smtk/common/TypeName.h"

#include <string>

namespace smtk
{
namespace markup
{
namespace json
{

/**\brief A helper for serialization of markup resources.
  *
  * This is needed in order for components to be deserialized
  * as they cannot be constructed without access to the resource
  * which will own them (and which cannot be provided to them
  * through the from_json method).
  *
  * It is also used to provide access to application data
  * through the smtk::common::Managers::Ptr it can hold.
  */
class SMTKCORE_EXPORT Helper
{
public:
  /// JSON data type
  using json = nlohmann::json;

  /// Destructor is public, but you shouldn't use it.
  ~Helper();

  /// Return the helper "singleton".
  ///
  /// The object returned is a per-thread instance
  /// at the top of a stack that may be altered using
  /// the pushInstance() and popInstance() methods.
  /// This allows nested deserializers to each have
  /// their own context that appears to be globally
  /// available.
  static Helper& instance();

  /// Push a new helper instance on the local thread's stack.
  ///
  /// The returned \a Helper will have:
  /// + The same managers as the previous (if any) helper.
  /// + The \a parent markup is assigned the ID 0.
  static Helper& pushInstance(const std::shared_ptr<smtk::markup::Resource>& resource);

  /// Pop a helper instance off the local thread's stack.
  static void popInstance();

  /// Return the nesting level (i.e., the number of helper instances in the stack).
  ///
  /// The outermost helper will return 1 (assuming you have called instance() first).
  static std::size_t nestingDepth();

  /// Set/get the managers to use when serializing/deserializing.
  ///
  /// Call setManagers() with an instance of all your application's
  /// managers before attempting to serialize/deserialize as helpers
  /// are allowed to use managers as needed.
  void setManagers(const smtk::common::Managers::Ptr& managers);
  smtk::common::Managers::Ptr managers();

  /// Return the current resource being (de)serialized.
  std::shared_ptr<smtk::markup::Resource> resource() const;

  /// Reset the helper's state.
  ///
  /// This should be called before beginning serialization or deserialization.
  /// Additionally, calling it after each of these tasks is recommended since
  /// it will free memory.
  void clear();

protected:
  Helper();
  std::weak_ptr<smtk::markup::Resource> m_resource;
  smtk::common::Managers::Ptr m_managers;
  /// m_topLevel indicates whether pushInstance() (false) or instance() (true)
  /// was used to create this helper.
  bool m_topLevel = true;
};

} // namespace json
} // namespace markup
} // namespace smtk

#endif // smtk_markup_json_Helper_h
