// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/AnalyticShape.h"

namespace smtk
{
namespace markup
{

AnalyticShape::~AnalyticShape() = default;

void AnalyticShape::initialize(const nlohmann::json& data, json::Helper& helper)
{
}

} // namespace markup
} // namespace smtk
