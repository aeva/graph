// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_ImplicitShape_h
#define smtk_markup_ImplicitShape_h

#include "smtk/markup/AnalyticShape.h"

#include "vtkSmartPointer.h"

class vtkImplicitFunction;
namespace smtk {namespace markup { class Domain; } }

namespace smtk
{
namespace markup
{

/// A shape described by a function defined on a domain.
class SMTKMARKUP_EXPORT ImplicitShape : public smtk::markup::AnalyticShape
{
public:
  smtkTypeMacro(smtk::markup::ImplicitShape);
  smtkSuperclassMacro(smtk::markup::AnalyticShape);

  template<typename... Args>
  ImplicitShape(Args&&... args)
    : smtk::markup::AnalyticShape(std::forward<Args>(args)...)
  {
  }

  ~ImplicitShape() override;

  /// Provide an initializer for resources to call after construction.
  void initialize(const nlohmann::json& data, json::Helper& helper) override;

  bool setDomain(const std::weak_ptr<smtk::markup::Domain>& domain);
  const std::weak_ptr<smtk::markup::Domain>& domain() const;
  std::weak_ptr<smtk::markup::Domain>& domain();

  bool setFunction(const vtkSmartPointer<vtkImplicitFunction>& function);
  const vtkSmartPointer<vtkImplicitFunction>& function() const;
  vtkSmartPointer<vtkImplicitFunction>& function();

protected:
  std::weak_ptr<smtk::markup::Domain> m_domain;
  vtkSmartPointer<vtkImplicitFunction> m_function;
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_ImplicitShape_h
