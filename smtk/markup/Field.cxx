// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/Field.h"

#include "smtk/markup/AssignedIds.h"
#include "smtk/markup/Index.h"

namespace smtk
{
namespace markup
{

Field::~Field() = default;

void Field::initialize(const nlohmann::json& data, json::Helper& helper)
{
}

std::string Field::name() const
{
  return m_name.data();
}

bool Field::setName(const smtk::string::Token& name)
{
  if (m_name == name)
  {
    return false;
  }
  m_name = name;
  return true;
}

bool Field::setType(const smtk::string::Token& type)
{
  if (m_type == type)
  {
    return false;
  }
  m_type = type;
  return true;
}

const smtk::string::Token& Field::type() const
{
  return m_type;
}

smtk::string::Token& Field::type()
{
  return m_type;
}

bool Field::setOrder(const std::vector<int>& order)
{
  if (m_order == order)
  {
    return false;
  }
  m_order = order;
  return true;
}

const std::vector<int>& Field::order() const
{
  return m_order;
}

std::vector<int>& Field::order()
{
  return m_order;
}

bool Field::setIds(const std::weak_ptr<smtk::markup::AssignedIds>& ids)
{
  auto mlocked = m_ids.lock();
  auto vlocked = ids.lock();
  if (mlocked == vlocked)
  {
    return false;
  }
  m_ids = vlocked;
  return true;
}

const std::weak_ptr<smtk::markup::AssignedIds>& Field::ids() const
{
  return m_ids;
}

std::weak_ptr<smtk::markup::AssignedIds>& Field::ids()
{
  return m_ids;
}

bool Field::setLookup(const std::weak_ptr<smtk::markup::Index>& lookup)
{
  auto mlocked = m_lookup.lock();
  auto vlocked = lookup.lock();
  if (mlocked == vlocked)
  {
    return false;
  }
  m_lookup = vlocked;
  return true;
}

const std::weak_ptr<smtk::markup::Index>& Field::lookup() const
{
  return m_lookup;
}

std::weak_ptr<smtk::markup::Index>& Field::lookup()
{
  return m_lookup;
}


} // namespace markup
} // namespace smtk
