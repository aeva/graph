// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/SurfaceGrid.h"

#include "vtkPolyData.h"

namespace smtk
{
namespace markup
{

SurfaceGrid::~SurfaceGrid() = default;

void SurfaceGrid::initialize(const nlohmann::json& data, json::Helper& helper)
{
}

bool SurfaceGrid::setData(const vtkSmartPointer<vtkPolyData>& data)
{
  if (m_data == data)
  {
    return false;
  }
  m_data = data;
  return true;
}

const vtkSmartPointer<vtkPolyData>& SurfaceGrid::data() const
{
  return m_data;
}

vtkSmartPointer<vtkPolyData>& SurfaceGrid::data()
{
  return m_data;
}


} // namespace markup
} // namespace smtk
