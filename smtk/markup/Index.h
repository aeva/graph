// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_Index_h
#define smtk_markup_Index_h

#include "smtk/markup/Domain.h"

namespace smtk
{
namespace markup
{

/// A catalog that orders numbers in an IdSpace according to some field. Example: A map from a range of Field values to an IdSpace representing points of a dataset.
class SMTKMARKUP_EXPORT Index : public smtk::markup::Domain
{
public:
  smtkTypeMacro(smtk::markup::Index);
  smtkSuperclassMacro(smtk::markup::Domain);

  template<typename... Args>
  Index(Args&&... args)
    : smtk::markup::Domain(std::forward<Args>(args)...)
  {
  }

  ~Index() override;

protected:
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_Index_h
