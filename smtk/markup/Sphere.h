// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_Sphere_h
#define smtk_markup_Sphere_h

#include "smtk/markup/AnalyticShape.h"

namespace smtk
{
namespace markup
{

/// A 3-dimensional surface of constant non-zero curvature identified by a point and radius.
class SMTKMARKUP_EXPORT Sphere : public smtk::markup::AnalyticShape
{
public:
  smtkTypeMacro(smtk::markup::Sphere);
  smtkSuperclassMacro(smtk::markup::AnalyticShape);

  template<typename... Args>
  Sphere(Args&&... args)
    : smtk::markup::AnalyticShape(std::forward<Args>(args)...)
  {
  }

  ~Sphere() override;

  /// Provide an initializer for resources to call after construction.
  void initialize(const nlohmann::json& data, json::Helper& helper) override;

  bool setCenter(const std::array<double, 3>& center);
  const std::array<double, 3>& center() const;
  std::array<double, 3>& center();

  bool setRadius(const std::array<double, 3>& radius);
  const std::array<double, 3>& radius() const;
  std::array<double, 3>& radius();

protected:
  std::array<double, 3> m_center;
  std::array<double, 3> m_radius;
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_Sphere_h
