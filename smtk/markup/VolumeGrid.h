// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_VolumeGrid_h
#define smtk_markup_VolumeGrid_h

#include "smtk/markup/Grid.h"

#include "vtkSmartPointer.h"

class vtkUnstructuredGrid;

namespace smtk
{
namespace markup
{

/// A collection of unstructured, possibly-nonconforming cells of any dimension.
class SMTKMARKUP_EXPORT VolumeGrid : public smtk::markup::Grid
{
public:
  smtkTypeMacro(smtk::markup::VolumeGrid);
  smtkSuperclassMacro(smtk::markup::Grid);

  template<typename... Args>
  VolumeGrid(Args&&... args)
    : smtk::markup::Grid(std::forward<Args>(args)...)
  {
  }

  ~VolumeGrid() override;

  /// Provide an initializer for resources to call after construction.
  void initialize(const nlohmann::json& data, json::Helper& helper) override;

  bool setData(const vtkSmartPointer<vtkUnstructuredGrid>& data);
  const vtkSmartPointer<vtkUnstructuredGrid>& data() const;
  vtkSmartPointer<vtkUnstructuredGrid>& data();

protected:
  vtkSmartPointer<vtkUnstructuredGrid> m_data;
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_VolumeGrid_h
