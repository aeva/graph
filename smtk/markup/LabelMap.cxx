// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/LabelMap.h"

namespace smtk
{
namespace markup
{

LabelMap::~LabelMap() = default;

void LabelMap::initialize(const nlohmann::json& data, json::Helper& helper)
{
}

bool LabelMap::setNames(const std::unordered_map<smtk::markup::AssignedIds::IdType, smtk::string::Token>& names)
{
  if (m_names == names)
  {
    return false;
  }
  m_names = names;
  return true;
}

const std::unordered_map<smtk::markup::AssignedIds::IdType, smtk::string::Token>& LabelMap::names() const
{
  return m_names;
}

std::unordered_map<smtk::markup::AssignedIds::IdType, smtk::string::Token>& LabelMap::names()
{
  return m_names;
}


} // namespace markup
} // namespace smtk
