// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_BoundaryOperator_h
#define smtk_markup_BoundaryOperator_h

#include "smtk/markup/Domain.h"

namespace smtk
{
namespace markup
{

/// A discrete mapping from one IdSpace into another that enumerates boundaries of the domain.
class SMTKMARKUP_EXPORT BoundaryOperator : public smtk::markup::Domain
{
public:
  smtkTypeMacro(smtk::markup::BoundaryOperator);
  smtkSuperclassMacro(smtk::markup::Domain);

  BoundaryOperator() = default;
  BoundaryOperator(smtk::string::Token name);
  BoundaryOperator(const nlohmann::json& data);
  ~BoundaryOperator() override = default;

protected:
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_BoundaryOperator_h
