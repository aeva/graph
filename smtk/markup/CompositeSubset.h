// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_CompositeSubset_h
#define smtk_markup_CompositeSubset_h

#include "smtk/markup/Subset.h"

namespace smtk {namespace markup { class Subset; } }

namespace smtk
{
namespace markup
{

/// A subset whose members are stored as other subsets.
class SMTKMARKUP_EXPORT CompositeSubset : public smtk::markup::Subset
{
public:
  smtkTypeMacro(smtk::markup::CompositeSubset);
  smtkSuperclassMacro(smtk::markup::Subset);

  template<typename... Args>
  CompositeSubset(Args&&... args)
    : smtk::markup::Subset(std::forward<Args>(args)...)
  {
  }

  ~CompositeSubset() override;

  bool setMembers(const std::set<std::weak_ptr<smtk::markup::Subset>, std::owner_less<std::weak_ptr<smtk::markup::Subset>>>& members);
  const std::set<std::weak_ptr<smtk::markup::Subset>, std::owner_less<std::weak_ptr<smtk::markup::Subset>>>& members() const;
  std::set<std::weak_ptr<smtk::markup::Subset>, std::owner_less<std::weak_ptr<smtk::markup::Subset>>>& members();

protected:
  std::set<std::weak_ptr<smtk::markup::Subset>, std::owner_less<std::weak_ptr<smtk::markup::Subset>>> m_members;
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_CompositeSubset_h
