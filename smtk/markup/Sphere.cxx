// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/Sphere.h"

namespace smtk
{
namespace markup
{

Sphere::~Sphere() = default;

void Sphere::initialize(const nlohmann::json& data, json::Helper& helper)
{
}

bool Sphere::setCenter(const std::array<double, 3>& center)
{
  if (m_center == center)
  {
    return false;
  }
  m_center = center;
  return true;
}

const std::array<double, 3>& Sphere::center() const
{
  return m_center;
}

std::array<double, 3>& Sphere::center()
{
  return m_center;
}

bool Sphere::setRadius(const std::array<double, 3>& radius)
{
  if (m_radius == radius)
  {
    return false;
  }
  m_radius = radius;
  return true;
}

const std::array<double, 3>& Sphere::radius() const
{
  return m_radius;
}

std::array<double, 3>& Sphere::radius()
{
  return m_radius;
}


} // namespace markup
} // namespace smtk
