// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/SpatialData.h"

namespace smtk
{
namespace markup
{

SpatialData::~SpatialData() = default;

void SpatialData::initialize(const nlohmann::json& data, json::Helper& helper)
{
}


} // namespace markup
} // namespace smtk
