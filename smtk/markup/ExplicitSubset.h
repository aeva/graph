// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_ExplicitSubset_h
#define smtk_markup_ExplicitSubset_h

#include "smtk/markup/Subset.h"

namespace smtk
{
namespace markup
{

/// A subset whose members are explicitly listed.
class SMTKMARKUP_EXPORT ExplicitSubset : public smtk::markup::Subset
{
public:
  smtkTypeMacro(smtk::markup::ExplicitSubset);
  smtkSuperclassMacro(smtk::markup::Subset);

  template<typename... Args>
  ExplicitSubset(Args&&... args)
    : smtk::markup::Subset(std::forward<Args>(args)...)
  {
  }

  ~ExplicitSubset() override;

  /// Provide an initializer for resources to call after construction.
  void initialize(const nlohmann::json& data, json::Helper& helper) override;

  bool setMembers(const std::set<smtk::markup::AssignedIds::IdType>& members);
  const std::set<smtk::markup::AssignedIds::IdType>& members() const;
  std::set<smtk::markup::AssignedIds::IdType>& members();

protected:
  std::set<smtk::markup::AssignedIds::IdType> m_members;
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_ExplicitSubset_h
