// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_Field_h
#define smtk_markup_Field_h

#include "smtk/markup/Component.h"

#include <string>
#include "smtk/string/Token.h"

namespace smtk {namespace markup { class AssignedIds; } }
namespace smtk {namespace markup { class Index; } }

namespace smtk
{
namespace markup
{

class SMTKMARKUP_EXPORT Field : public smtk::markup::Component
{
public:
  smtkTypeMacro(smtk::markup::Field);
  smtkSuperclassMacro(smtk::markup::Component);

  template<typename... Args>
  Field(Args&&... args)
    : smtk::markup::Component(std::forward<Args>(args)...)
  {
  }

  ~Field() override;

  /// Provide an initializer for resources to call after construction.
  void initialize(const nlohmann::json& data, json::Helper& helper) override;

  std::string name() const;

  bool setName(const smtk::string::Token& name);

  bool setType(const smtk::string::Token& type);
  const smtk::string::Token& type() const;
  smtk::string::Token& type();

  bool setOrder(const std::vector<int>& order);
  const std::vector<int>& order() const;
  std::vector<int>& order();

  bool setIds(const std::weak_ptr<smtk::markup::AssignedIds>& ids);
  const std::weak_ptr<smtk::markup::AssignedIds>& ids() const;
  std::weak_ptr<smtk::markup::AssignedIds>& ids();

  bool setLookup(const std::weak_ptr<smtk::markup::Index>& lookup);
  const std::weak_ptr<smtk::markup::Index>& lookup() const;
  std::weak_ptr<smtk::markup::Index>& lookup();

protected:
  smtk::string::Token m_name;
  smtk::string::Token m_type;
  std::vector<int> m_order;
  std::weak_ptr<smtk::markup::AssignedIds> m_ids;
  std::weak_ptr<smtk::markup::Index> m_lookup;
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_Field_h
