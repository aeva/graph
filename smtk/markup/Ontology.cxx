// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/Ontology.h"

namespace smtk
{
namespace markup
{

Ontology::~Ontology() = default;

void Ontology::initialize(const nlohmann::json& data, json::Helper& helper)
{
}

bool Ontology::setUrl(const std::string& url)
{
  if (m_url == url)
  {
    return false;
  }
  m_url = url;
  return true;
}

const std::string& Ontology::url() const
{
  return m_url;
}

std::string& Ontology::url()
{
  return m_url;
}


} // namespace markup
} // namespace smtk
