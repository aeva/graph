// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_Frame_h
#define smtk_markup_Frame_h

#include "smtk/markup/SpatialData.h"

namespace smtk
{
namespace markup
{

/// A coordinate frame (i.e., a set of orthonormal basis vectors that span a space).
class SMTKMARKUP_EXPORT Frame : public smtk::markup::SpatialData
{
public:
  smtkTypeMacro(smtk::markup::Frame);
  smtkSuperclassMacro(smtk::markup::SpatialData);

  template<typename... Args>
  Frame(Args&&... args)
    : smtk::markup::SpatialData(std::forward<Args>(args)...)
  {
  }

  ~Frame() override;

  /// Provide an initializer for resources to call after construction.
  void initialize(const nlohmann::json& data, json::Helper& helper) override;

  bool setAxes(const std::vector<std::vector<double>>& axes);
  const std::vector<std::vector<double>>& axes() const;
  std::vector<std::vector<double>>& axes();

protected:
  std::vector<std::vector<double>> m_axes;
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_Frame_h
