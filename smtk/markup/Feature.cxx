// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/Feature.h"

namespace smtk
{
namespace markup
{

Feature::~Feature() = default;


} // namespace markup
} // namespace smtk
