// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/References.h"

namespace smtk
{
namespace markup
{

References::~References() = default;

void References::initialize(const nlohmann::json& data, json::Helper& helper)
{
}

bool References::setKeys(const std::weak_ptr<smtk::markup::IdAllotment>& keys)
{
  auto mlocked = m_keys.lock();
  auto vlocked = keys.lock();
  if (mlocked == vlocked)
  {
    return false;
  }
  m_keys = vlocked;
  return true;
}

const std::weak_ptr<smtk::markup::IdAllotment>& References::keys() const
{
  return m_keys;
}

std::weak_ptr<smtk::markup::IdAllotment>& References::keys()
{
  return m_keys;
}

bool References::setMembers(const std::unordered_map<smtk::markup::AssignedIds::IdType, std::weak_ptr<smtk::markup::Component>>& members)
{
  if (m_members == members)
  {
    return false;
  }
  m_members = members;
  return true;
}

const std::unordered_map<smtk::markup::AssignedIds::IdType, std::weak_ptr<smtk::markup::Component>>& References::members() const
{
  return m_members;
}

std::unordered_map<smtk::markup::AssignedIds::IdType, std::weak_ptr<smtk::markup::Component>>& References::members()
{
  return m_members;
}


} // namespace markup
} // namespace smtk
