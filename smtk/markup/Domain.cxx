// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/Domain.h"

#include "smtk/string/json/jsonToken.h"

namespace smtk
{
namespace markup
{

Domain::Domain(smtk::string::Token name)
  : m_name(name)
{
}

Domain::Domain(const nlohmann::json& data)
  : m_name(data.at("name").get<smtk::string::Token>())
{
}

} // namespace markup
} // namespace smtk
