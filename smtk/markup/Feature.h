// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_Feature_h
#define smtk_markup_Feature_h

#include "smtk/markup/Component.h"

namespace smtk
{
namespace markup
{

class SMTKMARKUP_EXPORT Feature : public smtk::markup::Component
{
public:
  smtkTypeMacro(smtk::markup::Feature);
  smtkSuperclassMacro(smtk::markup::Component);

  template<typename... Args>
  Feature(Args&&... args)
    : smtk::markup::Component(std::forward<Args>(args)...)
  {
  }

  ~Feature() override;

protected:
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_Feature_h
