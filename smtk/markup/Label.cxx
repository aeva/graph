// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/Label.h"

namespace smtk
{
namespace markup
{

Label::~Label() = default;

void Label::initialize(const nlohmann::json& data, json::Helper& helper)
{
}


} // namespace markup
} // namespace smtk
