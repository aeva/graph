// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/Segmentation.h"

namespace smtk
{
namespace markup
{

Segmentation::~Segmentation() = default;

void Segmentation::initialize(const nlohmann::json& data, json::Helper& helper)
{
}


} // namespace markup
} // namespace smtk
