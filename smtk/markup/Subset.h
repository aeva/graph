// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_Subset_h
#define smtk_markup_Subset_h

#include "smtk/markup/SpatialData.h"

#include "smtk/markup/AssignedIds.h"

namespace smtk
{
namespace markup
{

/// A subset of an IdSpace. Subsets may represent cell or node subsets. See SideSet for subsets that involve the boundary operator.
class SMTKMARKUP_EXPORT Subset : public smtk::markup::SpatialData
{
public:
  smtkTypeMacro(smtk::markup::Subset);
  smtkSuperclassMacro(smtk::markup::SpatialData);

  using Visitor = std::function<smtk::common::Visit(smtk::markup::IdSpace&, smtk::markup::AssignedIds::IdType)>;

  template<typename... Args>
  Subset(Args&&... args)
    : smtk::markup::SpatialData(std::forward<Args>(args)...)
  {
  }

  ~Subset() override;

  /// Provide an initializer for resources to call after construction.
  void initialize(const nlohmann::json& data, json::Helper& helper) override;

  /// The signature of functions that visit elements of a subset.
  smtk::common::Visit visit(Visitor visitor);

  /// The IdSpace which is being subsetted.
  bool setIds(const std::weak_ptr<smtk::markup::AssignedIds>& ids);
  const std::weak_ptr<smtk::markup::AssignedIds>& ids() const;
  std::weak_ptr<smtk::markup::AssignedIds>& ids();

protected:
  std::weak_ptr<smtk::markup::AssignedIds> m_ids;
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_Subset_h
