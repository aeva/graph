// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/BoundaryOperator.h"

namespace smtk
{
namespace markup
{

BoundaryOperator::BoundaryOperator(smtk::string::Token name)
  : Domain(name)
{
  // TODO: create mapping?
}

BoundaryOperator::BoundaryOperator(const nlohmann::json& data)
  : Domain(data)
{
  // TODO: Deserialize or create mapping
}

} // namespace markup
} // namespace smtk
