// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/Box.h"

namespace smtk
{
namespace markup
{

Box::~Box() = default;

void Box::initialize(const nlohmann::json& data, json::Helper& helper)
{
}

bool Box::setRange(const std::array<std::array<double, 3>, 2>& range)
{
  if (m_range == range)
  {
    return false;
  }
  m_range = range;
  return true;
}

const std::array<std::array<double, 3>, 2>& Box::range() const
{
  return m_range;
}

std::array<std::array<double, 3>, 2>& Box::range()
{
  return m_range;
}


} // namespace markup
} // namespace smtk
