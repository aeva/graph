// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/Collection.h"

namespace smtk
{
namespace markup
{

Collection::~Collection() = default;

bool Collection::setMembers(const std::set<std::weak_ptr<smtk::markup::Component>, std::owner_less<std::weak_ptr<smtk::markup::Component>>>& members)
{
  std::unordered_map<smtk::markup::AssignedIds::IdType, std::weak_ptr<smtk::markup::Component>> next;
  std::unordered_set<smtk::markup::AssignedIds::IdType> removed;
  auto newEntries = members;
  for (const auto& entry : m_members)
  {
    if (auto member = entry.second.lock())
    {
      if (members.find(member) != members.end())
      {
        next[entry.first] = member;
        newEntries.erase(member);
      }
      else
      {
        removed.insert(entry.first);
      }
    }
  }
  std::size_t ii = 0;
  for (const auto& member : newEntries)
  {
    for (; next.find(ii) != next.end(); ++ii)
    { /* do nothing */ }
    next[ii] = member.lock();
    ++ii;
  }
  m_members = next;
  return removed.empty() && newEntries.empty();
}

bool Collection::setKeys(const std::weak_ptr<smtk::markup::IdAllotment>& keys)
{
  auto mlocked = m_keys.lock();
  auto vlocked = keys.lock();
  if (mlocked == vlocked)
  {
    return false;
  }
  m_keys = vlocked;
  return true;
}

const std::weak_ptr<smtk::markup::IdAllotment>& Collection::keys() const
{
  return m_keys;
}

std::weak_ptr<smtk::markup::IdAllotment>& Collection::keys()
{
  return m_keys;
}

const std::unordered_map<smtk::markup::AssignedIds::IdType, std::weak_ptr<smtk::markup::Component>>& Collection::members() const
{
  return m_members;
}

std::unordered_map<smtk::markup::AssignedIds::IdType, std::weak_ptr<smtk::markup::Component>>& Collection::members()
{
  return m_members;
}


} // namespace markup
} // namespace smtk
