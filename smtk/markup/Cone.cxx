// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/Cone.h"

namespace smtk
{
namespace markup
{

Cone::~Cone() = default;

void Cone::initialize(const nlohmann::json& data, json::Helper& helper)
{
}

bool Cone::setEndpoints(const std::array<std::array<double, 3>, 2>& endpoints)
{
  if (m_endpoints == endpoints)
  {
    return false;
  }
  m_endpoints = endpoints;
  return true;
}

const std::array<std::array<double, 3>, 2>& Cone::endpoints() const
{
  return m_endpoints;
}

std::array<std::array<double, 3>, 2>& Cone::endpoints()
{
  return m_endpoints;
}

bool Cone::setRadii(const std::array<double, 2>& radii)
{
  if (m_radii == radii)
  {
    return false;
  }
  m_radii = radii;
  return true;
}

const std::array<double, 2>& Cone::radii() const
{
  return m_radii;
}

std::array<double, 2>& Cone::radii()
{
  return m_radii;
}


} // namespace markup
} // namespace smtk
