// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_AssignedIds_h
#define smtk_markup_AssignedIds_h

#include "smtk/markup/Component.h"

#include "smtk/common/Visit.h"

namespace smtk
{
namespace markup
{

class IdSpace;

/// The nature of how identifiers in an instance of AssignedIds are used.
enum Nature
{
  Primary,      //!< Identifiers are exclusively owned by the \a component.
  Referential,  //!< Identifiers are not owned – only referenced – by the \a component.
  NonExclusive, //!< Ownership of identifiers is shared with other components.
  Unassigned    //!< Identifiers are not reserved for use.
};


/// An API for querying the IDs allotted to a component in an IdSpace.
class SMTKMARKUP_EXPORT AssignedIds : public std::enable_shared_from_this<AssignedIds>
{
public:
  smtkTypeMacroBase(smtk::markup::AssignedIds);

  using IdType = std::size_t;
  using IdIterator = struct { smtk::markup::IdSpace* idSpace; IdType begin; IdType end; Nature nature; };
  using IdRange = std::array<IdType, 2>; // struct { smtk::markup::IdSpace* idSpace; IdType begin; IdType end; };
  using Iterable = std::function<bool(IdIterator&)>;
  using ContainsFunctor = std::function<std::size_t(const IdRange&)>;
  using Visitor = std::function<smtk::common::Visit(smtk::markup::IdSpace&, IdType)>;

  template<typename... Args>
  AssignedIds(const std::shared_ptr<IdSpace>& space, Nature nature, IdType begin, IdType end, Args&&... args)
    : m_space(space)
    , m_range(IdRange{begin, end})
    , m_nature(nature)
  {
  }

  virtual ~AssignedIds();

  /// Returns the range of IDs in the allotment.
  IdRange range() const;
  /// Returns a functor to iterate all assigned IDs. Call the functor until it returns false.
  Iterable iterable() const;
  /// Returns functor to query the number of allotted ids in the half-open interval [begin, end[.
  ContainsFunctor contains() const;
  /// Call \a visitor on each alloted ID.
  smtk::common::Visit visit(Visitor visitor) const;

  std::shared_ptr<smtk::markup::IdSpace> space();

  bool setNature(const Nature& nature);
  const Nature& nature() const;
  Nature& nature();

protected:
  std::weak_ptr<smtk::markup::IdSpace> m_space;
  IdRange m_range;
  Nature m_nature;
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_AssignedIds_h
