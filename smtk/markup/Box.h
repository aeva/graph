// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_Box_h
#define smtk_markup_Box_h

#include "smtk/markup/AnalyticShape.h"

namespace smtk
{
namespace markup
{

/// A 3-dimensional, 6-sided shape with axis-aligned planar surfaces.
class SMTKMARKUP_EXPORT Box : public smtk::markup::AnalyticShape
{
public:
  smtkTypeMacro(smtk::markup::Box);
  smtkSuperclassMacro(smtk::markup::AnalyticShape);

  template<typename... Args>
  Box(Args&&... args)
    : smtk::markup::AnalyticShape(std::forward<Args>(args)...)
  {
  }

  ~Box() override;

  /// Provide an initializer for resources to call after construction.
  void initialize(const nlohmann::json& data, json::Helper& helper) override;

  bool setRange(const std::array<std::array<double, 3>, 2>& range);
  const std::array<std::array<double, 3>, 2>& range() const;
  std::array<std::array<double, 3>, 2>& range();

protected:
  std::array<std::array<double, 3>, 2> m_range;
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_Box_h
