// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_OntologyIdentifier_h
#define smtk_markup_OntologyIdentifier_h

#include "smtk/markup/Label.h"

#include "smtk/markup/Ontology.h"
#include "smtk/string/Token.h"

namespace smtk
{
namespace markup
{

class SMTKMARKUP_EXPORT OntologyIdentifier : public smtk::markup::Label
{
public:
  smtkTypeMacro(smtk::markup::OntologyIdentifier);
  smtkSuperclassMacro(smtk::markup::Label);

  using Token = smtk::string::Token;

  template<typename... Args>
  OntologyIdentifier(Args&&... args)
    : smtk::markup::Label(std::forward<Args>(args)...)
  {
  }

  ~OntologyIdentifier() override;

  /// Provide an initializer for resources to call after construction.
  void initialize(const nlohmann::json& data, json::Helper& helper) override;

  /// A URL in the ontologys schema that can be queried for relationships.
  bool setOntologyId(const OntologyIdentifier::Token& ontologyId);
  const OntologyIdentifier::Token& ontologyId() const;
  OntologyIdentifier::Token& ontologyId();

  const smtk::markup::Ontology& ontology() const;
  smtk::markup::Ontology& ontology();
  bool setontology(const smtk::markup::Ontology&);

protected:
  OntologyIdentifier::Token m_ontologyId;
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_OntologyIdentifier_h
