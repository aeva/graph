// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/Index.h"

namespace smtk
{
namespace markup
{

Index::~Index() = default;


} // namespace markup
} // namespace smtk
