// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_Domain_h
#define smtk_markup_Domain_h

#include "smtk/markup/Exports.h"

#include "smtk/SharedFromThis.h"
#include "smtk/string/Token.h"

#include "nlohmann/json.hpp"

#include <memory>

namespace smtk
{
namespace markup
{

/// The domain of a discrete or parameterized dataset.
class SMTKMARKUP_EXPORT Domain : public std::enable_shared_from_this<Domain>
{
public:
  smtkTypeMacroBase(smtk::markup::Domain);

  Domain() = default;
  Domain(smtk::string::Token name);
  Domain(const nlohmann::json& data);
  virtual ~Domain() = default;

  smtk::string::Token name() const { return m_name; }

protected:
  smtk::string::Token m_name;
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_Domain_h
