set(sources)
set(headers
  arcs/Labels.h

  queries/SelectionFootprint.h
)
set(operations
  Create
  Read
  SetName
  Write
)

list(APPEND classes
  Resource
  DomainMap

  Registrar

  AssignedIds
  Component
  Domain
  BoundaryOperator
  IdSpace
  ParameterSpace
  Index
  IdAllotment
  Collection
  Ontology
  Frame
  Group
  AnalyticShape
  Plane
  Sphere
  Cone
  Box
  ImplicitShape
  DiscreteGeometry
  Image
  Grid
  VolumeGrid
  SurfaceGrid
  Subset
  ExplicitSubset
  RangedSubset
  CompositeSubset
  SideSet
  Field
  LabelMap
  Segmentation
  SpatialData
  Label
  OntologyIdentifier
  Comment
  Feature
  Landmark

  detail/NodeContainer

  json/Helper
  json/jsonResource
  json/jsonComponent
  json/jsonDomain
  json/jsonDomainMap
  # arcs/Children<NodeType, bool Hold>
  # arcs/Parents<NodeType>
  # arcs/DerivedData
)

# NB: Keep the next two foreach loops in order!
# Encode operation SBT files into headers and operations into classes.
foreach(operation ${operations})
  list(APPEND classes "operators/${operation}")
  smtk_encode_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/operators/${operation}.sbt"
    TARGET_OUTPUT targetName
  )
  list(APPEND markupDependencies ${targetName})
endforeach()

# Turn "classes" into source and headers entries
foreach(class ${classes})
  list(APPEND headers ${class}.h)
  list(APPEND sources ${class}.cxx)
endforeach()

add_library(smtkMarkup SHARED ${sources} ${headers})
smtk_export_header(smtkMarkup Exports.h)
add_dependencies(smtkMarkup ${markupDependencies})
target_link_libraries(smtkMarkup
  PUBLIC
    smtkCore
    smtkPVServerExt
    vtkSMTKSourceExt
    VTK::AEVAExt
    VTK::MeshingNetGen
    nglib
    Boost::boost
  PRIVATE
    VTK::CommonCore
    VTK::CommonDataModel
    VTK::IOImage
    VTK::IOXML
    VTK::IOGeometry
    VTK::FiltersCore
    VTK::FiltersExtraction
    VTK::FiltersGeometry
    VTK::FiltersParallelDIY2
    VTK::FiltersPoints
    smtkPQComponentsExt
    ${ITK_LIBRARIES}
    ${__dependencies}
    Boost::filesystem
    VTK::hdf5
)

if (AEVA_ENABLE_TESTING)
  add_subdirectory(testing/cxx)
endif()
