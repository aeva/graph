// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_Segmentation_h
#define smtk_markup_Segmentation_h

#include "smtk/markup/LabelMap.h"

namespace smtk
{
namespace markup
{

/// A label map that contains only 2 or 3 labels: 0 (outside), 1 (inside), and optionally 2 (on)
class SMTKMARKUP_EXPORT Segmentation : public smtk::markup::LabelMap
{
public:
  smtkTypeMacro(smtk::markup::Segmentation);
  smtkSuperclassMacro(smtk::markup::LabelMap);

  template<typename... Args>
  Segmentation(Args&&... args)
    : smtk::markup::LabelMap(std::forward<Args>(args)...)
  {
  }

  ~Segmentation() override;

  /// Provide an initializer for resources to call after construction.
  void initialize(const nlohmann::json& data, json::Helper& helper) override;

protected:
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_Segmentation_h
