// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).

#ifndef smtk_markup_NodeFactory_h
#define smtk_markup_NodeFactory_h

#include "smtk/markup/Component.h"

#include "smtk/common/Factory.h"
#include "smtk/common/UUID.h"

#include "nlohmann/json.hpp"

namespace smtk
{
namespace markup
{

class Resource;

using NodeFactory =
  smtk::common::Factory<
    smtk::markup::Component,
    // Possible constructor arguments:
    // 1. Just a resource-pointer:
    std::shared_ptr<smtk::markup::Resource>,
    // 2. A resource-pointer and a UUID
    smtk::common::factory::Inputs<
      std::shared_ptr<smtk::markup::Resource>,
      smtk::common::UUID
    >
  >;

} // namespace markup
} // namespace smtk

#endif // smtk_markup_NodeFactory_h
