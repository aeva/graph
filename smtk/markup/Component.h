// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_Component_h
#define smtk_markup_Component_h

#include "smtk/markup/Exports.h"

#include "smtk/graph/Component.h"

#include "smtk/markup/ArcEditor.h"

#include "nlohmann/json.hpp"

namespace smtk
{
namespace markup
{
namespace json { class Helper; }

class Group;
class Label;

class SMTKMARKUP_EXPORT Component : public smtk::graph::Component
{
public:
  smtkTypeMacro(smtk::markup::Component);
  smtkSuperclassMacro(smtk::graph::Component);

  using Index = std::size_t;

  template<typename... Args>
  Component(Args&&... args)
    : smtk::graph::Component(std::forward<Args>(args)...)
  {
  }
  ~Component() override;

  /// Provide an initializer for resources to call after construction.
  virtual void initialize(const nlohmann::json& data, json::Helper& helper);

  /// The index is a compile-time intrinsic of the derived resource; as such, it cannot be set.
  Component::Index index() const;

  /// Return the component's name.
  std::string name() const override;

  /// Set the component's name.
  bool setName(const std::string& name);

  /// A unique integer corresponding to the component type.
  static const Component::Index type_index;

  ArcEditor<SelfType, smtk::markup::Group> groups() const;

  ArcEditor<SelfType, smtk::markup::Label> labels() const;

protected:
  /// A functor for changing the name of a component.
  struct ModifyName;

  std::string m_name;
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_Component_h
