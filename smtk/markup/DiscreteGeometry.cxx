// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/DiscreteGeometry.h"

namespace smtk
{
namespace markup
{

DiscreteGeometry::~DiscreteGeometry() = default;


} // namespace markup
} // namespace smtk
