// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_ArcEditor_h
#define smtk_markup_ArcEditor_h

#include "smtk/markup/Exports.h"
#include "smtk/graph/arcs/Arcs.h"

namespace smtk
{
namespace markup
{

/**\brief A class to iterate over and edit arcs in a graph resource.
  *
  * Of note, this class supports insertion and removal during iteration.
  *
  * Examples:
  * ```c++
  * // Ranged for-loops are supported:
  * std::shared_ptr<Group> group;
  * for (const auto& member : group->members())
  * {
  *   std::cout << "  Member "<< member.name() << "\n";
  * }
  *
  * // The editor also presents a stateful API for iteration
  * auto members = group->members();
  * for (; !members.done(); ++members)
  * {
  *   if (members->name() == "foo")
  *   {
  *     // It is possible to erase the current arc during iteration.
  *     members.erase();
  *   }
  * }
  * ```
  */
template<typename SourceNodeType, typename NodeType>
class SMTKMARKUP_EXPORT ArcEditor
{
public:
  NodeType& operator () ();
  NodeType& operator * ();
  NodeType& operator -> ();
  bool operator ++ ();
  bool done() const;

  /// Return true when comparing whether two arc editors reference the same node.
  ///
  /// When \a other == this, then we only return true when the iterator is at
  /// the end of iteration.
  bool operator == (const ArcEditor<SourceNodeType, NodeType>& other)
  {
    if (&other == this)
    {
      // only return true when at end.
      return this->done();
    }
    return (*this)() == other();
  }
  const ArcEditor<SourceNodeType, NodeType>& begin() { return *this; }
  const ArcEditor<SourceNodeType, NodeType>& end() { return *this; }
protected:
  // TODO:
  // typename smtk::graph::Arcs<SourceNodeType, NodeType>::API& m_api;
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_ArcEditor_h
