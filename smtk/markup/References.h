// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#ifndef smtk_markup_References_h
#define smtk_markup_References_h

#include "smtk/markup/Component.h"

namespace smtk
{
namespace markup
{

/// A set of weak references to components.
class SMTKMARKUP_EXPORT References : public smtk::markup::Component
{
public:
  smtkTypeMacro(smtk::markup::References);
  smtkSuperclassMacro(smtk::markup::Component);

  template<typename... Args>
  References(Args&&... args)
    : smtk::markup::Component(std::forward<Args>(args)...)
  {
  }

  ~References() override;

  /// Provide an initializer for resources to call after construction.
  void initialize(const nlohmann::json& data, json::Helper& helper) override;

  bool setKeys(const std::weak_ptr<smtk::markup::IdAllotment>& keys);
  const std::weak_ptr<smtk::markup::IdAllotment>& keys() const;
  std::weak_ptr<smtk::markup::IdAllotment>& keys();

  bool setMembers(const std::unordered_map<smtk::markup::AssignedIds::IdType, std::weak_ptr<smtk::markup::Component>>& members);
  const std::unordered_map<smtk::markup::AssignedIds::IdType, std::weak_ptr<smtk::markup::Component>>& members() const;
  std::unordered_map<smtk::markup::AssignedIds::IdType, std::weak_ptr<smtk::markup::Component>>& members();

protected:
  std::weak_ptr<smtk::markup::IdAllotment> m_keys;
  std::unordered_map<smtk::markup::AssignedIds::IdType, std::weak_ptr<smtk::markup::Component>> m_members;
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_References_h
