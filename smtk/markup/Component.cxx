// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/Component.h"

#include "smtk/markup/Group.h"
#include "smtk/markup/Label.h"
#include "smtk/markup/Resource.h"
#include "smtk/markup/json/Helper.h"

namespace smtk
{
namespace markup
{

struct Component::ModifyName
{
  ModifyName(const std::string& nextName)
    : m_name(nextName)
  {
  }

  void operator()(Component::Ptr& c)
  {
    c->m_name = m_name;
  }

  std::string m_name;
};

Component::~Component() = default;

void Component::initialize(const nlohmann::json& data, json::Helper& helper)
{
  (void)helper;
  std::cout << "init comp " << this << " with " << data.dump(2) << "\n";
  auto it = data.find("name");
  if (it != data.end())
  {
    this->setName(it->get<std::string>());
  }
}

Component::Index Component::index() const
{
  return std::type_index(typeid(*this)).hash_code();}

std::string Component::name() const
{
  return m_name;
}

bool Component::setName(const std::string& name)
{
  if (name == m_name)
  {
    return false;
  }

  auto owner = std::dynamic_pointer_cast<smtk::markup::Resource>(this->resource());
  if (owner)
  {
    // We are not allowed to set our name directly since
    // our owning resource has indexed us by name.
    return owner->modifyComponent(*this, ModifyName(name));
  }
  m_name = name;
  return true;
}


} // namespace markup
} // namespace smtk
