// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_LabelMap_h
#define smtk_markup_LabelMap_h

#include "smtk/markup/Field.h"

#include "smtk/markup/IdSpace.h"

namespace smtk
{
namespace markup
{

/// A mapping from an IdSpace to an integer label, with optional string names for each integer.
class SMTKMARKUP_EXPORT LabelMap : public smtk::markup::Field
{
public:
  smtkTypeMacro(smtk::markup::LabelMap);
  smtkSuperclassMacro(smtk::markup::Field);

  template<typename... Args>
  LabelMap(Args&&... args)
    : smtk::markup::Field(std::forward<Args>(args)...)
  {
  }

  ~LabelMap() override;

  /// Provide an initializer for resources to call after construction.
  void initialize(const nlohmann::json& data, json::Helper& helper) override;

  /// A set of labels that form the image of the label-map.
  bool setNames(const std::unordered_map<smtk::markup::AssignedIds::IdType, smtk::string::Token>& names);
  const std::unordered_map<smtk::markup::AssignedIds::IdType, smtk::string::Token>& names() const;
  std::unordered_map<smtk::markup::AssignedIds::IdType, smtk::string::Token>& names();

protected:
  std::unordered_map<smtk::markup::AssignedIds::IdType, smtk::string::Token> m_names;
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_LabelMap_h
