// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).

#ifndef smtk_markup_Traits_h
#define smtk_markup_Traits_h
/*!\file */

#include "smtk/markup/Exports.h"

// #include "smtk/markup/arcs/ChildrenAs.h"
// #include "smtk/markup/arcs/ParentsAs.h"

#include "smtk/markup/AnalyticShape.h"
#include "smtk/markup/AssignedIds.h"
#include "smtk/markup/BoundaryOperator.h"
#include "smtk/markup/Box.h"
#include "smtk/markup/Collection.h"
#include "smtk/markup/Comment.h"
#include "smtk/markup/Component.h"
#include "smtk/markup/CompositeSubset.h"
#include "smtk/markup/Cone.h"
#include "smtk/markup/DiscreteGeometry.h"
#include "smtk/markup/Domain.h"
#include "smtk/markup/ExplicitSubset.h"
#include "smtk/markup/Feature.h"
#include "smtk/markup/Field.h"
#include "smtk/markup/Frame.h"
#include "smtk/markup/Grid.h"
#include "smtk/markup/Group.h"
#include "smtk/markup/IdAllotment.h"
#include "smtk/markup/IdSpace.h"
#include "smtk/markup/Image.h"
#include "smtk/markup/ImplicitShape.h"
#include "smtk/markup/Index.h"
#include "smtk/markup/Label.h"
#include "smtk/markup/LabelMap.h"
#include "smtk/markup/Landmark.h"
#include "smtk/markup/Ontology.h"
#include "smtk/markup/OntologyIdentifier.h"
#include "smtk/markup/ParameterSpace.h"
#include "smtk/markup/Plane.h"
#include "smtk/markup/RangedSubset.h"
#include "smtk/markup/References.h"
#include "smtk/markup/Segmentation.h"
#include "smtk/markup/SideSet.h"
#include "smtk/markup/SpatialData.h"
#include "smtk/markup/Sphere.h"
#include "smtk/markup/Subset.h"
#include "smtk/markup/SurfaceGrid.h"
#include "smtk/markup/VolumeGrid.h"
#include "smtk/markup/arcs/Labels.h"
#include "smtk/markup/detail/NodeContainer.h"

#include <tuple>

namespace smtk
{
namespace markup
{

/**\brief Traits that describe markup node and arc types.
  *
  */
struct SMTKMARKUP_EXPORT Traits
{
  using NodeTypes = std::tuple<
    AnalyticShape,
    // AssignedIds,
    Box,
    Collection,
    Comment,
    Component,
    CompositeSubset,
    Cone,
    DiscreteGeometry,
    // Domain,
    ExplicitSubset,
    Feature,
    Field,
    Frame,
    Grid,
    Group,
    // IdAllotment,
    Image,
    ImplicitShape,
    Label,
    LabelMap,
    Landmark,
    Ontology,
    OntologyIdentifier,
    Plane,
    RangedSubset,
    // References,
    Segmentation,
    SideSet,
    SpatialData,
    Sphere,
    Subset,
    SurfaceGrid,
    VolumeGrid
  >;
  using ArcTypes = std::tuple<
    Labels,
    LabelSubject
  >;
  /*
  typedef std::tuple<ChildrenAs<CompSolid>, ChildrenAs<Solid>, ChildrenAs<Shell>, ChildrenAs<Face>,
    ChildrenAs<Wire>, ChildrenAs<Edge>, ChildrenAs<Vertex>, Children, ParentsAs<Compound>,
    ParentsAs<CompSolid>, ParentsAs<Solid>, ParentsAs<Shell>, ParentsAs<Face>, ParentsAs<Wire>,
    ParentsAs<Edge>, Parents>
    ArcTypes;
    */
  using NodeContainer = detail::NodeContainer;
  using DomainTypes = std::tuple<
    BoundaryOperator,
    IdSpace,
    Index,
    ParameterSpace
  >;
};
}
}

#endif
