// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_DiscreteGeometry_h
#define smtk_markup_DiscreteGeometry_h

#include "smtk/markup/SpatialData.h"

namespace smtk
{
namespace markup
{

class SMTKMARKUP_EXPORT DiscreteGeometry : public smtk::markup::SpatialData
{
public:
  smtkTypeMacro(smtk::markup::DiscreteGeometry);
  smtkSuperclassMacro(smtk::markup::SpatialData);

  template<typename... Args>
  DiscreteGeometry(Args&&... args)
    : smtk::markup::SpatialData(std::forward<Args>(args)...)
  {
  }

  ~DiscreteGeometry() override;

protected:
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_DiscreteGeometry_h
