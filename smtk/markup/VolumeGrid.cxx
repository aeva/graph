// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/VolumeGrid.h"

#include "vtkUnstructuredGrid.h"

namespace smtk
{
namespace markup
{

VolumeGrid::~VolumeGrid() = default;

void VolumeGrid::initialize(const nlohmann::json& data, json::Helper& helper)
{
}

bool VolumeGrid::setData(const vtkSmartPointer<vtkUnstructuredGrid>& data)
{
  if (m_data == data)
  {
    return false;
  }
  m_data = data;
  return true;
}

const vtkSmartPointer<vtkUnstructuredGrid>& VolumeGrid::data() const
{
  return m_data;
}

vtkSmartPointer<vtkUnstructuredGrid>& VolumeGrid::data()
{
  return m_data;
}


} // namespace markup
} // namespace smtk
