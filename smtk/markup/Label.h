// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_Label_h
#define smtk_markup_Label_h

#include "smtk/markup/Component.h"

#include "smtk/markup/ArcEditor.h"

namespace smtk
{
namespace markup
{

class SMTKMARKUP_EXPORT Label : public smtk::markup::Component
{
public:
  smtkTypeMacro(smtk::markup::Label);
  smtkSuperclassMacro(smtk::markup::Component);

  template<typename... Args>
  Label(Args&&... args)
    : smtk::markup::Component(std::forward<Args>(args)...)
  {
  }

  ~Label() override;

  /// Provide an initializer for resources to call after construction.
  void initialize(const nlohmann::json& data, json::Helper& helper) override;

  ArcEditor<SelfType, smtk::markup::Component> subject() const;

protected:
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_Label_h
