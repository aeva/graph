// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_IdAllotment_h
#define smtk_markup_IdAllotment_h

#include "smtk/markup/Component.h"

#include "smtk/common/Visit.h"
#include "smtk/markup/IdSpace.h"

namespace smtk
{
namespace markup
{

/// An API for querying the IDs allotted to a component in an IdSpace.
class SMTKMARKUP_EXPORT IdAllotment : public smtk::markup::Component
{
public:
  smtkTypeMacro(smtk::markup::IdAllotment);
  smtkSuperclassMacro(smtk::markup::Component);

  enum Nature
{ Primary,Referential,NonExclusive };

  using IdType = smtk::markup::AssignedIds::IdType;
  using Visitor = std::function<smtk::common::Visit(smtk::markup::IdSpace&, IdType)>;

  template<typename... Args>
  IdAllotment(Args&&... args)
    : smtk::markup::Component(std::forward<Args>(args)...)
  {
  }

  ~IdAllotment() override;

  std::array<IdAllotment::IdType, 2> range() const;
  std::size_t contains(IdType begin, IdType end) const;
  smtk::common::Visit visit(Visitor visitor) const;

  bool setIdSpace(const std::weak_ptr<smtk::markup::IdSpace>& idSpace);
  const std::weak_ptr<smtk::markup::IdSpace>& idSpace() const;
  std::weak_ptr<smtk::markup::IdSpace>& idSpace();

  bool setNature(const IdAllotment::Nature& nature);
  const IdAllotment::Nature& nature() const;
  IdAllotment::Nature& nature();

protected:
  std::weak_ptr<smtk::markup::IdSpace> m_idSpace;
  IdAllotment::Nature m_nature;
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_IdAllotment_h
