// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_arcs_Labels_h
#define smtk_markup_arcs_Labels_h

#include "smtk/markup/Exports.h"
#include "smtk/graph/arcs/Arc.h"
#include "smtk/graph/arcs/Arcs.h"

namespace smtk
{
namespace markup
{

// Node types
class Component;
class Label;

// Forward declare arc types
class Labels;
class LabelSubject;

class SMTKMARKUP_EXPORT Labels : public smtk::graph::Arcs<Component, Label, Labels>
{
public:
  using InverseArcType = LabelSubject;
  using smtk::graph::Arcs<Component, Label, Labels>::Arcs;
};

class SMTKMARKUP_EXPORT LabelSubject : public smtk::graph::Arc<Label, Component, LabelSubject>
{
public:
  using InverseArcType = Labels;
  using smtk::graph::Arc<Label, Component, LabelSubject>::Arc;
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_arcs_Labels_h
