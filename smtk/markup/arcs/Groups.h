// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef smtk_markup_Groups_h
#define smtk_markup_Groups_h

#include "smtk/markup/Exports.h"

namespace smtk
{
namespace markup
{

// Node types
class Component;
class Group;

// Forward declare arc types
class Groups; // 1 Component → N Group
class Members; // 1 Group → M Component

class SMTKMARKUP_EXPORT Groups : public smtk::graph::Arcs<Component, Group, Groups>
{
public:
  using InverseArcType = Members;
  using smtk::graph::Arcs<Component, Group, Groups>::Arcs;
};

class SMTKMARKUP_EXPORT Members : public smtk::graph::Arcs<Group, Component, Members>
{
public:
  using InverseArcType = Groups;
  using smtk::graph::Arcs<Group, Component, Members>::Arcs;
};

} // namespace markup
} // namespace smtk

#endif // smtk_markup_Groups_h
