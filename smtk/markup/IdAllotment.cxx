// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/IdAllotment.h"

namespace smtk
{
namespace markup
{

IdAllotment::~IdAllotment() = default;

bool IdAllotment::setIdSpace(const std::weak_ptr<smtk::markup::IdSpace>& idSpace)
{
  auto mlocked = m_idSpace.lock();
  auto vlocked = idSpace.lock();
  if (mlocked == vlocked)
  {
    return false;
  }
  m_idSpace = vlocked;
  return true;
}

const std::weak_ptr<smtk::markup::IdSpace>& IdAllotment::idSpace() const
{
  return m_idSpace;
}

std::weak_ptr<smtk::markup::IdSpace>& IdAllotment::idSpace()
{
  return m_idSpace;
}

bool IdAllotment::setNature(const IdAllotment::Nature& nature)
{
  if (m_nature == nature)
  {
    return false;
  }
  m_nature = nature;
  return true;
}

const IdAllotment::Nature& IdAllotment::nature() const
{
  return m_nature;
}

IdAllotment::Nature& IdAllotment::nature()
{
  return m_nature;
}


} // namespace markup
} // namespace smtk
