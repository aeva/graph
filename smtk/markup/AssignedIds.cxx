// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/AssignedIds.h"

#include "smtk/markup/IdSpace.h"

namespace smtk
{
namespace markup
{

AssignedIds::~AssignedIds()
{
  // Always remove ourselves from our containing IdSpace.
  if (auto idspace = m_space.lock())
  {
    idspace->removeEntry(*this);
  }
}

AssignedIds::IdRange AssignedIds::range() const
{
  return m_range;
}

#if 0
/// Returns a functor to iterate all assigned IDs. Call the functor until it returns false.
Iterable AssignedIds::iterable() const;
  /// Returns functor to query the number of allotted ids in the half-open interval [begin, end[.
ContainsFunctor AssignedIds::contains() const;
  /// Call \a visitor on each alloted ID.
smtk::common::Visit visit(Visitor visitor) const;
#endif

std::shared_ptr<smtk::markup::IdSpace> AssignedIds::space()
{
  return m_space.lock();
}

bool AssignedIds::setNature(const Nature& nature)
{
  if (m_nature == nature)
  {
    return false;
  }
  m_nature = nature;
  return true;
}

const Nature& AssignedIds::nature() const
{
  return m_nature;
}

Nature& AssignedIds::nature()
{
  return m_nature;
}


} // namespace markup
} // namespace smtk
