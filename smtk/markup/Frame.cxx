// Copyright © Kitware Inc under the [BSD-3-clause license](https://kitware.com/licenses/bsd.md).
#include "smtk/markup/Frame.h"

namespace smtk
{
namespace markup
{

Frame::~Frame() = default;

void Frame::initialize(const nlohmann::json& data, json::Helper& helper)
{
}

bool Frame::setAxes(const std::vector<std::vector<double>>& axes)
{
  if (m_axes == axes)
  {
    return false;
  }
  m_axes = axes;
  return true;
}

const std::vector<std::vector<double>>& Frame::axes() const
{
  return m_axes;
}

std::vector<std::vector<double>>& Frame::axes()
{
  return m_axes;
}


} // namespace markup
} // namespace smtk
