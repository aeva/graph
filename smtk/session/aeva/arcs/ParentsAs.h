//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_session_aeva_ParentsAs_h
#define smtk_session_aeva_ParentsAs_h

#include "smtk/graph/Resource.h"
#include "smtk/session/aeva/nodes/Annotation.h"

#include "smtk/common/CompilerInformation.h"

namespace smtk
{
namespace session
{
namespace aeva
{

template <typename XToType>
class ParentsAs
{
public:
  typedef Annotation FromType;
  typedef XToType ToType;
  typedef std::vector<std::reference_wrapper<const ToType> > Container;

  ParentsAs(const FromType& from)
    : m_from(from)
  {
  }

  const FromType& from() const { return m_from; }
  Container to() const
  {
    Container container;
    this->visit([&container](const ToType& toType) {
      container.push_back(std::cref(toType));
      return false;
    });
    return container;
  }

  bool visit(const std::function<bool(const ToType&)>&) const;

  bool visit(const std::function<bool(ToType&)>&);

  template <typename SelfType>
  class API
  {
  protected:
    SelfType& self(const typename SelfType::FromType& lhs) const
    {
      auto& arcs = std::static_pointer_cast<smtk::graph::ResourceBase>(lhs.resource())->arcs();
      if (!arcs.template contains<SelfType>(lhs.id()))
      {
        arcs.template emplace<SelfType>(lhs.id(), lhs);
      }
      return arcs.template get<SelfType>().at(lhs.id());
    }

  public:
    Container get(const typename SelfType::FromType& lhs) const { return self(lhs).to(); }

    bool contains(const typename SelfType::FromType& lhs) const
    {
      return std::static_pointer_cast<smtk::graph::ResourceBase>(lhs.resource())
        ->arcs()
        .template get<SelfType>()
        .contains(lhs.id());
    }

    bool visit(const FromType& lhs, std::function<bool(const typename SelfType::ToType&)> fn) const
    {
      return self(lhs).visit(fn);
    }

    bool visit(const FromType& lhs, std::function<bool(typename SelfType::ToType&)> fn)
    {
      return self(lhs).visit(fn);
    }
  };

private:
  const FromType& m_from;
};

typedef ParentsAs<Annotation> Parents;
}
}
}

#endif
