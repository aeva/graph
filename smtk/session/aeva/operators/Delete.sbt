<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the "delete" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="delete" Label="Model - Delete Geometry" BaseType="operation">

      <AssociationsDef Name="entities" NumberOfRequiredValues="1" Extensible="true" HoldReference="true">
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter=""/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>
        <Void Name="delete dependents" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
          <BriefDescription>
            Should dependent components be deleted, or should the operation fail if there are dependents?
          </BriefDescription>
          <DetailedDescription>
            When set, any dependent components will be deleted rathert than causing the operation to fail.
            For example, if original geometry is queued for deletion but selections exist on that geometry,
            the operation will fail unless "delete dependents" is enabled. If it is enabled, then more
            components than were requested will be deleted.
          </DetailedDescription>
        </Void>

        <!-- CellSelection uses the "resource" item below to ensure the resource is not
          deleted in a separate thread before the operation runs (which can happen in
          tests).
          -->
        <Resource Name="resource" NumberOfRequiredValues="0" Extensible="true" HoldReference="true"/>

      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(delete)" BaseType="result">
      <ItemDefinitions>
        <Void Name="allow camera reset" IsEnabledByDefault="true" AdvanceLevel="11"/>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
