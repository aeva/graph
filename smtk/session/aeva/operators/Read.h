//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_session_aeva_Read_h
#define smtk_session_aeva_Read_h

#include "smtk/session/aeva/Operation.h"
#include "smtk/session/aeva/Resource.h"

#include "nlohmann/json.hpp"

namespace smtk
{
namespace session
{
namespace aeva
{

/**\brief Read an aeva resource.
  */
class SMTKAEVASESSION_EXPORT Read : public Operation
{
public:
  smtkTypeMacro(smtk::session::aeva::Read);
  smtkCreateMacro(Read);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtkSuperclassMacro(Operation);

protected:
  Result readLegacy(nlohmann::json& jdata, const std::shared_ptr<Resource>& rsrc);
  Result operateInternal() override;
  virtual const char* xmlDescription() const override;
  void markModifiedResources(Result&) override;
};

SMTKAEVASESSION_EXPORT smtk::resource::ResourcePtr read(
  const std::string& filename,
  const std::shared_ptr<smtk::common::Managers>& managers
);

}
}
}

#endif
