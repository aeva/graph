//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "Read.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"

#include "smtk/session/aeva/Read_xml.h"
#include "smtk/session/aeva/Resource.h"
#include "smtk/session/aeva/json/jsonResource.h"

#include "smtk/common/Paths.h"

#include "smtk/model/SessionIOJSON.h"

#include "vtkPolyData.h"
#include "vtkUnstructuredGrid.h"
#include "vtkXMLPolyDataReader.h"
#include "vtkXMLUnstructuredGridReader.h"

//force to use filesystem version 3
#define BOOST_FILESYSTEM_VERSION 3
#include <boost/filesystem.hpp>

#ifdef ERROR
#undef ERROR
#endif

namespace smtk
{
namespace session
{
namespace aeva
{

vtkSmartPointer<vtkDataSet> readVTKData(const std::string& filename)
{
  vtkSmartPointer<vtkDataSet> data;
  // distinguish polydata and unstructured grid by file extension
  std::string ext = smtk::common::Paths::extension(filename);
  if (ext == ".vtp")
  {
    vtkNew<vtkXMLPolyDataReader> reader;
    reader->SetFileName(filename.c_str());
    reader->Update();
    data = reader->GetOutput();
  }
  else if (ext == ".vtu")
  {
    vtkNew<vtkXMLUnstructuredGridReader> reader;
    reader->SetFileName(filename.c_str());
    reader->Update();
    data = reader->GetOutput();
  }
  return data;
}

Read::Result Read::readLegacy(nlohmann::json& jdata, const std::shared_ptr<Resource>& rsrc)
{
  // FIXME TODO
  return this->createResult(smtk::operation::Operation::Outcome::FAILED);
}

Read::Result Read::operateInternal()
{
  std::string filename = this->parameters()->findFile("filename")->value();

  // Load file and parse it:
  json jdata;
  std::ifstream file(filename);
  if (!file.good())
  {
    smtkErrorMacro(
      smtk::io::Logger::instance(), "Could not open \"" << filename << "\" for reading.");
  }
  else
  {
    try
    {
      jdata = json::parse(file);
    }
    catch (...)
    {
      smtkErrorMacro(
        smtk::io::Logger::instance(), "File \"" << filename << "\" is not in JSON format.");
    }
  }

  // Now convert the JSON records into aeva components:
  if (jdata.is_null())
  {
    smtkErrorMacro(log(), "Cannot parse file \"" << filename << "\".");
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);
  }

  // Create a new resource for the import
  auto rsrc = smtk::session::aeva::Resource::create();
  rsrc->setLocation(filename);

  // Read legacy (model-based, not graph-based) aeva data
  // Fetch the ordered list of model UUIDs corresponding to each json record.
  json preservedUUIDs;
  try
  {
    preservedUUIDs = jdata.at("preservedUUIDs");
    return this->readLegacy(jdata, rsrc);
  }
  catch (...)
  {
    // do nothing
  }

  try
  {
    smtk::session::aeva::from_json(jdata, rsrc);
  }
  catch (...)
  {
    smtkErrorMacro(log(), "Cannot process file \"" << filename << "\".");
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);
  }

  Result result = this->createResult(smtk::operation::Operation::Outcome::SUCCEEDED);
  {
    smtk::attribute::ResourceItem::Ptr created = result->findResource("resource");
    created->setValue(rsrc);
  }

  return result;
}

const char* Read::xmlDescription() const
{
  return Read_xml;
}

void Read::markModifiedResources(Read::Result& res)
{
  auto resourceItem = res->findResource("resource");
  for (auto rit = resourceItem->begin(); rit != resourceItem->end(); ++rit)
  {
    auto resource = std::dynamic_pointer_cast<smtk::resource::Resource>(*rit);

    // Set the resource as unmodified from its persistent (i.e. on-disk) state
    resource->setClean(true);
  }
}

smtk::resource::ResourcePtr read(
  const std::string& filename,
  const std::shared_ptr<smtk::common::Managers>& managers)
{
  Read::Ptr read = Read::create();
  read->setManagers(managers);
  read->parameters()->findFile("filename")->setValue(filename);
  Read::Result result = read->operate();
  if (result->findInt("outcome")->value() != static_cast<int>(Read::Outcome::SUCCEEDED))
  {
    return smtk::resource::ResourcePtr();
  }
  return result->findResource("resource")->value();
}

}
}
}
