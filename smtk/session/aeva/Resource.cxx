// Copyright (c) Kitware, Inc. All rights reserved. See license.md for details.
#include "smtk/io/Logger.h"

#include "smtk/session/aeva/Resource.h"

namespace smtk
{
namespace session
{
namespace aeva
{

Resource::Resource(const smtk::common::UUID& uid, smtk::resource::ManagerPtr const& manager)
  : smtk::resource::DerivedFrom<Resource, smtk::graph::Resource<Traits>>(uid, manager)
{
}

Resource::Resource(smtk::resource::ManagerPtr const& manager)
  : smtk::resource::DerivedFrom<Resource, smtk::graph::Resource<Traits>>(manager)
{
}

Resource::~Resource() = default;

smtk::resource::ComponentPtr Resource::find(const smtk::common::UUID& compId) const
{
  return this->Superclass::find(compId);
}

} // namespace aeva
} // namespace session
} // namespace smtk
