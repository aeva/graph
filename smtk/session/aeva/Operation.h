//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_aeva_Operation_h
#define smtk_session_aeva_Operation_h

#include "smtk/attribute/ReferenceItem.h"
#include "smtk/operation/XMLOperation.h"
#include "smtk/session/aeva/Exports.h"

#include "vtkSmartPointer.h"

class vtkDataObject;

namespace smtk
{
namespace session
{
namespace aeva
{

class Resource;
class Session;
typedef smtk::shared_ptr<Session> SessionPtr;
struct EntityHandle;

/**\brief An operator using the AEVA "kernel."
  *
  * This is a base class for actual operators.
  * It provides convenience methods for accessing AEVA-specific data
  * for its subclasses to use internally.
  */
class SMTKAEVASESSION_EXPORT Operation : public smtk::operation::XMLOperation
{
public:
  /// Return the VTK data object (if any) defining the given component.
  ///
  /// Note that this need not be the same as the data object returned by
  /// the resource's Geometry object.
  static vtkSmartPointer<vtkDataObject> storage(const smtk::resource::PersistentObject*);
  static vtkSmartPointer<vtkDataObject> storage(
    const std::shared_ptr<smtk::resource::PersistentObject>&);

protected:
  /// Return true if the given reference item holds values that
  /// have VTK data in the session and false otherwise.
  static bool allValuesHaveStorage(const smtk::attribute::ReferenceItem& item);

  static vtkSmartPointer<vtkDataObject> getData(const smtk::resource::PersistentObjectPtr& ptr);
};

} // namespace aeva
} // namespace session
} // namespace smtk

#endif
