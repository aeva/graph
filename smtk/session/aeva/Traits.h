//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_aeva_Traits_h
#define smtk_session_aeva_Traits_h
/*!\file */

#include "smtk/session/aeva/Exports.h"

#include "smtk/session/aeva/arcs/ChildrenAs.h"
#include "smtk/session/aeva/arcs/ParentsAs.h"

#include "smtk/session/aeva/nodes/Annotation.h"

#include <tuple>

namespace smtk
{
namespace session
{
namespace aeva
{

/**\brief Traits that describe aeva node and arc types.
  *
  */
struct SMTKAEVASESSION_EXPORT Traits
{
  typedef std::tuple<
    Annotation
    > NodeTypes;
  typedef std::tuple<
    ChildrenAs<Annotation>,
    Children,
    ParentsAs<Annotation>,
    Parents
    > ArcTypes;
};

}
}
}

#endif
