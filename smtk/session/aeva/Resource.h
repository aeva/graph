// Copyright (c) Kitware, Inc. All rights reserved. See license.md for details.
#ifndef smtk_session_aeva_Resource_h
#define smtk_session_aeva_Resource_h
/*!\file */

#include "smtk/graph/Resource.h"
#include "smtk/resource/DerivedFrom.h"
#include "smtk/resource/Manager.h"

#include "smtk/session/aeva/Traits.h"

namespace smtk
{
namespace session
{
namespace aeva
{

// Forward-declare node types
// I. Base types
class Annotation;
class PrimaryAnnotation;
class ReferenceAnnotation;

// II. Grouping types:
class Collection;
class References;

// III. Primary shape types: (the geometry defining the shape is owned)
class Image;
class Volume;
class Surface;

// IV. Reference shape types: (the geometry defining the shape is referenced)
class SubSet;
class SideSet;
class NodeSet;

// V. Non-geometric annotations.
class Frame;
class Field;
class Segmentation;
class Label;
class Tissue;
class Organ;
class Feature;
class Landmark;

/// A resource for anatomical annotation.
class SMTKAEVASESSION_EXPORT Resource
  : public smtk::resource::DerivedFrom<Resource, smtk::graph::Resource<Traits>>
{
public:
  smtkTypeMacro(smtk::session::aeva::Resource);
  smtkSuperclassMacro(smtk::resource::DerivedFrom<Resource, smtk::graph::Resource<Traits>>);
  smtkSharedPtrCreateMacro(smtk::resource::Resource);

  virtual ~Resource();

  /// We override find() in order to return primitive-selection data not explicitly
  /// held by the resource (and thus not serialized).
  smtk::resource::ComponentPtr find(const smtk::common::UUID& compId) const override;

  /// Set/get this resource's top-level modeling object
  Collection* root() { return m_root.get(); }
  void setRoot(const std::shared_ptr<Collection>& root) { m_root = root; }

  // wrap to avoid name conflict in msvc
  template <typename componentT>
  smtk::shared_ptr<componentT> createNode()
  {
    return smtk::graph::Resource<Traits>::create<componentT>();
  }

protected:
  Resource(const smtk::common::UUID&, smtk::resource::Manager::Ptr const& manager = nullptr);
  Resource(smtk::resource::Manager::Ptr const& manager = nullptr);

  std::shared_ptr<Collection> m_root;
};

} // namespace aeva
} // namespace session
} // namespace smtk
#endif // smtk_session_aeva_Resource_h
