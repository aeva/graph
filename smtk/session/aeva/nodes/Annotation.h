//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_session_aeva_Annotation_h
#define smtk_session_aeva_Annotation_h

#include "smtk/graph/Component.h"

#include "smtk/resource/Properties.h"

#include "smtk/session/aeva/Exports.h"

#include "vtkSmartPointer.h"

class vtkDataObject;

namespace smtk
{

/**\brief A return value that visitors should use to allow early termination.
  *
  * Note that if a visitor is called in parallel, the visitor may be invoked
  * in other threads even after one thread's visitor returns ShouldHalt::Yes.
  */
enum class ShouldHalt
{
  Yes, // Do not invoke the visitor any more.
  No   // Continue invoking the visitor.
};

namespace session
{
namespace aeva
{

class Resource;

/**\brief The basic entity of aeva is an annotation.
  *
  * Annotations do not have requirements placed on the form they take
  * or their relationship to other annotations in the resource.
  * However, because VTK is the medium by which we represent nearly all
  * geometric objects, we force annotations to provide a uniform interface
  * to that data (if any is present).
  * Similarly, we force annotations to provide a method to visit children
  * even though there is no requirement that they have any.
  */
class SMTKAEVASESSION_EXPORT Annotation : public smtk::graph::Component
{
public:
  using Visitor = std::function<ShouldHalt(Annotation*)>;

  smtkTypeMacro(Annotation);
  smtkSuperclassMacro(smtk::graph::Component);

  Annotation(const std::shared_ptr<smtk::graph::ResourceBase>& rsrc)
    : Component(rsrc)
  {
  }

  std::string name() const override
  {
    const auto& props = this->properties();
    if (props.contains<std::string>("name"))
    {
      return this->properties().at<std::string>("name");
    }
    return this->Superclass::name();
  }
  virtual void setName(const std::string& name)
  {
    this->properties().get<std::string>()["name"] = name;
  }

  virtual const vtkSmartPointer<vtkDataObject> data() const;
  virtual vtkSmartPointer<vtkDataObject> data();

  /// Return the parametric dimension of the annotation's geometry (or -1 if there is no geometry).
  virtual int dimension() const { return -1; }

  /// Return the parent resource as a session::aeva::Resource, not a resource::Resource.
  Resource* owningResource() const;

  /// Invoke \a visitor on every sub-component until visitor returns true (to terminate early).
  virtual void visitChildren(Visitor visitor)
  {
    // This base class does nothing.
  }
};
}
}
}

#endif
