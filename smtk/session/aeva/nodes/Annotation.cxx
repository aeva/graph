//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/session/aeva/Resource.h"
#include "smtk/session/aeva/nodes/Annotation.h"

#include "vtkDataObject.h"

namespace smtk
{
namespace session
{
namespace aeva
{

const vtkSmartPointer<vtkDataObject> Annotation::data() const
{
  return nullptr;
}

vtkSmartPointer<vtkDataObject> Annotation::data()
{
  return nullptr;
}

Resource* Annotation::owningResource() const
{
  return dynamic_cast<Resource*>(this->resource().get());
}

}
}
}
